package com.kyso84.android.demo.core.service

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.core.Const
import android.location.Criteria
import androidx.core.content.ContextCompat.getSystemService



class LocationService(private val context: Context) : LocationListener, LifecycleObserver {
    private val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private val liveLocation = MutableLiveData<Location>()
    val location: LiveData<Location> = liveLocation


    interface PermissionCallback {
        fun inciteToGrantPermission()
        fun onPermissionDeclined()
    }

    fun askLocation(fragment: Fragment, callback: PermissionCallback, force: Boolean = false) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            val provider = manager.getBestProvider(Criteria(), false)
            manager.requestLocationUpdates(provider, 1000, 10f, this)
        } else if (!ActivityCompat.shouldShowRequestPermissionRationale(fragment.context as Activity, Manifest.permission.ACCESS_FINE_LOCATION) || force) {
            fragment.requestPermissions(permissions, Const.REQUEST_PERMISSION_LOCATION)
        } else {
            callback.inciteToGrantPermission()
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray, callback: PermissionCallback) {
        if (requestCode == Const.REQUEST_PERMISSION_LOCATION) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                val provider = manager.getBestProvider(Criteria(), false)
                manager.requestLocationUpdates(provider, 1000, 10f, this)
            } else {
                callback.onPermissionDeclined()
            }
        }
    }

    override fun onLocationChanged(location: Location?) {
        liveLocation.value = location
        manager.removeUpdates(this)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}
}