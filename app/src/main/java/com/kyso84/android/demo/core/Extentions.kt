package com.kyso84.android.demo.core

import android.content.Context
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.R
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.model.Receipt
import java.lang.reflect.Field

fun Float?.toStringNonZero(): String = if (this != null && this != 0.0f) this.toString() else ""
fun Double?.toStringNonZero(): String = if (this != null && this != 0.0) this.toString() else ""
fun Int?.toStringNonZero(): String = if (this != null && this != 0) this.toString() else ""
fun Long?.toStringNonZero(): String = if (this != null && this != 0L) this.toString() else ""
fun Boolean?.toInt(): Int = if (this != null && this) 1 else 0

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}


fun List<Contact>.toEmailList() : List<String> {
    val list = mutableListOf<String>()
    this.forEach { list.add(it.email) }
    return list
}