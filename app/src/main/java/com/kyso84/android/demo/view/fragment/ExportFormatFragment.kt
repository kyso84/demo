package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentExportFormatBinding
import com.kyso84.android.demo.model.ExportFormat
import com.kyso84.android.demo.view.adapter.ExportFormatAdapter
import com.kyso84.android.demo.viewmodel.ExportViewModel

class ExportFormatFragment : BaseFragment<FragmentExportFormatBinding, ExportViewModel>(), PagerFragment {
    override fun getTitle() = R.string.format

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentExportFormatBinding>(inflater, R.layout.fragment_export_format, container, false).root
    }


    override fun onViewReady(viewModel: ExportViewModel, savedInstanceState: Bundle?) {
        val adapter = ExportFormatAdapter(layoutInflater)
        binding.fragmentExportFormatList.adapter = adapter
        binding.fragmentExportFormatList.layoutManager = LinearLayoutManager(context)


        adapter.setOnSelectionChangedListener(object: ExportFormatAdapter.OnSelectionChangedListener {
            override fun onSelectionChanged(exportFormat: ExportFormat) {
                viewModel.setExportFormat(exportFormat)
            }
        })
    }

    override fun getViewModelInstance(): ExportViewModel = ViewModelProviders.of(activity!!, this.viewModeFactory).get(ExportViewModel::class.java)

}