package com.kyso84.android.demo.injection.module

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.maps.GeoApiContext
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.datasource.CarRepository
import com.kyso84.android.demo.datasource.ContactRepository
import com.kyso84.android.demo.datasource.GeoRepository
import com.kyso84.android.demo.datasource.SystemRepository
import com.kyso84.android.demo.datasource.local.AppDao
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.datasource.remote.AppApi
import com.kyso84.android.demo.datasource.remote.CarApi
import com.kyso84.android.demo.datasource.repo.AppFirebaseDatabaseRepository
import com.kyso84.android.demo.datasource.repo.AppFirebaseFirestoreRepository
import com.kyso84.android.demo.datasource.repo.AppManagedRepository
import com.kyso84.android.demo.datasource.repo.AuthFirebaseRepository
import com.kyso84.android.demo.datasource.repo.AuthManagedRepository
import com.kyso84.android.demo.datasource.repo.DeviceRepository
import com.kyso84.android.demo.datasource.repo.GeoApiGoogleRepository
import com.kyso84.android.demo.datasource.repo.SystemFirebaseRepository
import com.kyso84.android.demo.datasource.repo.SystemManagedRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module()
class RepositoryModule {

    @Provides
    @Singleton
    fun provideGoogleGeoApiRepository(@Named(Const.KEY_API) apiKey: String, geoApiContext: GeoApiContext): GeoApiGoogleRepository = GeoApiGoogleRepository(apiKey, geoApiContext)

    @Provides
    @Singleton
    fun provideGeoRepository(googlePlaceRepository: GeoApiGoogleRepository): GeoRepository = googlePlaceRepository

    @Provides
    @Singleton
    fun provideCarRepository(carApi: CarApi) = CarRepository(carApi)

    @Provides
    @Singleton
    fun provideDeviceRepository(context: Context): DeviceRepository = DeviceRepository(context)

    @Provides
    @Singleton
    fun provideContactRepository(deviceRepository: DeviceRepository): ContactRepository = deviceRepository


    // System conf
    @Provides
    @Singleton
    fun provideSystemManagedRepository(@Named(Const.KEY_OPEN) api: AppApi, preferenceService: PreferenceService): SystemManagedRepository = SystemManagedRepository(api, preferenceService)

    @Provides
    @Singleton
    fun provideSystemFirebaseRepository(firebaseRemoteConfig: FirebaseRemoteConfig, preferenceService: PreferenceService): SystemFirebaseRepository = SystemFirebaseRepository(firebaseRemoteConfig, preferenceService)

    @Provides
    @Singleton
    fun provideSystemRepository(preferenceService: PreferenceService, managedRepository: SystemManagedRepository, firebaseRepository: SystemFirebaseRepository): SystemRepository =
            if (preferenceService.isSyncEnabled)
                firebaseRepository
            else
                managedRepository


    // Session source
    @Provides
    @Singleton
    fun provideAuthManagedRepository(@Named(Const.KEY_OPEN) api: AppApi, preferenceService: PreferenceService): AuthManagedRepository = AuthManagedRepository(api, preferenceService)

    @Provides
    @Singleton
    fun provideAuthFirebaseRepository(firebaseAuth: FirebaseAuth): AuthFirebaseRepository = AuthFirebaseRepository(firebaseAuth)

    @Provides
    @Singleton
    fun provideSessionRepository(preferenceService: PreferenceService, managedRepository: AuthManagedRepository, firebaseRepository: AuthFirebaseRepository): AuthRepository =
            if (preferenceService.isSyncEnabled) {
                firebaseRepository
            } else {
                managedRepository
            }




    @Provides
    @Singleton
    fun provideAppManagedRepository(authRepository: AuthRepository, @Named(Const.KEY_RESTRICTED)  api: AppApi, dao: AppDao): AppManagedRepository = AppManagedRepository(authRepository , api, dao)

    @Provides
    @Singleton
    fun provideAppFirebaseDatabaseRepository(authRepository: AuthRepository,firebaseDatabase: FirebaseDatabase): AppFirebaseDatabaseRepository = AppFirebaseDatabaseRepository(authRepository , firebaseDatabase)

    @Provides
    @Singleton
    fun provideAppFirebaseFirestoreRepository(authRepository: AuthRepository,firebaseFirestore: FirebaseFirestore): AppFirebaseFirestoreRepository = AppFirebaseFirestoreRepository(authRepository, firebaseFirestore)

    @Provides
    @Singleton
    fun provideAppRepository(preferenceService: PreferenceService, managedRepository: AppManagedRepository, firebaseDatabaseRepository: AppFirebaseDatabaseRepository, firebaseFirebaseRepository: AppFirebaseFirestoreRepository): AppRepository =
            if (preferenceService.isSyncEnabled) {
                if (preferenceService.isFirestore) {
                    firebaseFirebaseRepository
                } else {
                    firebaseDatabaseRepository
                }
            } else {
                managedRepository
            }



}