package com.kyso84.android.demo.injection

import android.app.Activity
import com.kyso84.android.demo.injection.module.ActivityModule
import com.kyso84.android.demo.injection.scope.ActivityScope
import dagger.BindsInstance
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun activity(activity: Activity): Builder
        fun appComponent(appComponent: AppComponent): Builder
        fun build(): ActivityComponent
    }

    fun inject(activity: Activity)
}
