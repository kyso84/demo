package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.databinding.PartExportFormatRowBinding
import com.kyso84.android.demo.model.ExportFormat

class ExportFormatViewHolder(val binding : PartExportFormatRowBinding) :RecyclerView.ViewHolder( binding.root) {
    constructor(layoutInflater: LayoutInflater, container: ViewGroup?):this(PartExportFormatRowBinding.inflate(layoutInflater, container, false))


    fun bind(format: ExportFormat, selected: Boolean?) {
        binding.partExportFormatRowTitle.setText(format.titleId)
        binding.partExportFormatRowIcon.setImageResource(format.iconId)
        selected?.let{
            binding.root.isSelected = it
            binding.partExportFormatRowTitle.isSelected = it
            binding.partExportFormatRowIcon.isSelected = it
        }
    }
}