package com.kyso84.android.demo.datasource

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataWrapper

interface AppRepository {
    fun addReceipt(receipt: Receipt): LiveData<DataWrapper<Receipt>>
    fun getReceiptList(): LiveData<DataWrapper<List<Receipt>>>
    fun addGasStation(gasStation: GasStation): LiveData<DataWrapper<GasStation>>
    fun getGasStation(gasStationId: String): LiveData<DataWrapper<GasStation>>
    fun getGasStationList(): LiveData<DataWrapper<List<GasStation>>>
    fun addCar(car: Car): LiveData<DataWrapper<Car>>
    fun getCar(carId: String): LiveData<DataWrapper<Car>>
    fun getLatestCar(): LiveData<DataWrapper<Car?>>
    fun getCarList(): LiveData<DataWrapper<List<Car>>>
}