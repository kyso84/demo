package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.kyso84.android.demo.core.base.BaseRecyclerAdapter
import com.kyso84.android.demo.core.base.BaseViewHolder
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.util.MultiSelectionManager

class ReceiptRecyclerAdapter(private val lifecycleOwner: LifecycleOwner, private val layoutInflater: LayoutInflater, private val multiSelectionManager: MultiSelectionManager? = null) : BaseRecyclerAdapter<Receipt, ReceiptRowViewHolder>() {
    constructor(activity: AppCompatActivity, multiSelectionManager: MultiSelectionManager? = null) : this(activity as LifecycleOwner, activity.layoutInflater, multiSelectionManager)
    constructor(fragment: Fragment, multiSelectionManager: MultiSelectionManager? = null) : this(fragment as LifecycleOwner, fragment.layoutInflater, multiSelectionManager)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ReceiptRowViewHolder(lifecycleOwner = lifecycleOwner, layoutInflater = layoutInflater, container = parent, multiSelectionManager = multiSelectionManager)

    override fun onBindViewHolder(holder: ReceiptRowViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    fun getSelectedItems(): List<Receipt> {
        val result = mutableListOf<Receipt>()
        multiSelectionManager?.let {
            it.getSelectedItems().forEach { position ->
                result.add(getItem(position))
            }
        }
        return result
    }
}