package com.kyso84.android.demo.core

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper


class SimpleItemTouchHelperCallback(private val adapter: TouchAdapter) : ItemTouchHelper.Callback() {

    interface TouchAdapter{
        /**
         * Called when an item has been dragged far enough to trigger a move. This is called every time
         * an item is shifted, and **not** at the end of a "drop" event.<br></br>
         * <br></br>
         * Implementations should call [RecyclerView.Adapter.notifyItemMoved] after
         * adjusting the underlying data to reflect this move.
         *
         * @param fromPosition The start position of the moved item.
         * @param toPosition   Then resolved position of the moved item.
         *
         * @see RecyclerView.getAdapterPositionFor
         * @see RecyclerView.ViewHolder.getAdapterPosition
         */
        fun onItemMove(fromPosition: Int, toPosition: Int)


        /**
         * Called when an item has been dismissed by a swipe.<br></br>
         * <br></br>
         * Implementations should call [RecyclerView.Adapter.notifyItemRemoved] after
         * adjusting the underlying data to reflect this removal.
         *
         * @param position The position of the item dismissed.
         *
         * @see RecyclerView.getAdapterPositionFor
         * @see RecyclerView.ViewHolder.getAdapterPosition
         */
        fun onItemDismiss(position: Int)
    }



    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return false
    }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: ViewHolder,  target: ViewHolder): Boolean {
        adapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        adapter.onItemDismiss(viewHolder.adapterPosition)
    }

}