package com.kyso84.android.demo.injection

import android.app.Application
import com.google.gson.Gson
import com.kyso84.android.demo.App
import com.kyso84.android.demo.injection.injector.ActivityInjectorModule
import com.kyso84.android.demo.injection.injector.FragmentInjectorModule
import com.kyso84.android.demo.injection.module.ApiModule
import com.kyso84.android.demo.injection.module.AppModule
import com.kyso84.android.demo.injection.module.FirebaseModule
import com.kyso84.android.demo.injection.module.NetworkModule
import com.kyso84.android.demo.injection.module.RepositoryModule
import com.kyso84.android.demo.injection.module.ServiceModule
import com.kyso84.android.demo.injection.module.StorageModule
import com.kyso84.android.demo.injection.module.ViewModelModule
import com.kyso84.android.demo.view.adapter.ReceiptRowViewHolder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    ApiModule::class,
    StorageModule::class,
    ViewModelModule::class,
    RepositoryModule::class,
    ServiceModule::class,
    FirebaseModule::class,
    // Android Specific
    ActivityInjectorModule::class,
    FragmentInjectorModule::class,
    AndroidSupportInjectionModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }


    fun inject(app: App)
    fun inject(receiptRowViewHolder: ReceiptRowViewHolder)

    fun getGson(): Gson
}
