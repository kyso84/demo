package com.kyso84.android.demo.core.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.kyso84.android.demo.BR
import com.kyso84.android.demo.injection.ActivityComponent
import com.kyso84.android.demo.injection.AppComponent
import com.kyso84.android.demo.injection.DaggerActivityComponent
import com.kyso84.android.demo.injection.ViewModelFactory
import com.quicklib.android.mvvm.view.QuickActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity<VDB : ViewDataBinding, VM : ViewModel> : QuickActivity<VDB, VM>(), HasSupportFragmentInjector {

    @Inject
    protected lateinit var androidFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var appComponent: AppComponent
    lateinit var activityComponent: ActivityComponent

    @Inject
    lateinit var viewModeFactory: ViewModelFactory

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = androidFragmentInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as HasActivityInjector).activityInjector().inject(this)
        activityComponent = DaggerActivityComponent.builder().appComponent(appComponent).activity(this).build()
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        binding.setVariable(BR.viewModel, viewModel)
        onViewReady(viewModel, savedInstanceState)
    }

    abstract fun onViewReady(viewModel: VM, savedInstanceState: Bundle?)
}
