package com.kyso84.android.demo.viewmodel

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.core.notifyObserver
import com.kyso84.android.demo.core.service.LocationService
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.GeoRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.GasType
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataStatus
import javax.inject.Inject

class ReceiptFormViewModel @Inject constructor(
        private val repository: AppRepository,
        private val preferenceService: PreferenceService,
        private val locationService: LocationService,
        private val geoRepository: GeoRepository
) : BaseViewModel() {

    private val liveReceipt = MediatorLiveData<Receipt>()
    val receipt: LiveData<Receipt>  = liveReceipt

    private val liveCar = MediatorLiveData<Car>()
    val car: LiveData<Car> = liveCar

    private val liveGasStation = MediatorLiveData<GasStation>()
    val gasStation: LiveData<GasStation> = liveGasStation

    private val liveEditable = MutableLiveData<Boolean>()
    val editable: LiveData<Boolean> = liveEditable

    fun start(oldReceipt: Receipt? = null, car: Car? = null, gasStation: GasStation? = null) {
        oldReceipt?.let {
            liveEditable.value = false
            liveReceipt.value = it
            onCarUpdated(it.carId)
            onGasStationUpdated(it.gasStationId)
        } ?: run {
            car?.let {
                new(it, gasStation)
            }
        }
    }

    fun new(car: Car, gasStation: GasStation? = null) {
        val newReceipt = Receipt()
        newReceipt.currencyCode = preferenceService.currency.currencyCode
        newReceipt.distanceUnit = preferenceService.distanceUnit
        newReceipt.volumeUnit = preferenceService.volumeUnit
        newReceipt.gasType = car.gasTypeGroup.gasTypeList.first()
        newReceipt.carId = car.id
        newReceipt.carName = car.toString()
        newReceipt.gasStationId = gasStation?.id
        newReceipt.gasStationName = gasStation.toString()
        liveEditable.value = true
        liveReceipt.value = newReceipt
        liveGasStation.value = gasStation
        liveCar.value = car
    }

    private fun getReceipt(): Receipt? = liveReceipt.value

    private fun updateTotal() {
        getReceipt()?.let {
            it.total = it.volume * it.price
        }
    }

    fun onGasTypeChange(gasType: GasType) {
        getReceipt()?.gasType = gasType
        liveReceipt.notifyObserver()
    }

    var odometerText:String = ""
    fun onOdometerFocusLost(){
        try {
            getReceipt()?.odometer = odometerText.toDouble()
        } catch (e: NumberFormatException) {
            getReceipt()?.odometer = 0.0
        }
        liveReceipt.notifyObserver()
    }
    fun onOdometerChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        odometerText = s.toString()
    }

    var volumeText:String = ""
    fun onVolumeFocusLost(){
        try {
            getReceipt()?.volume = volumeText.toDouble()
        } catch (e: NumberFormatException) {
            getReceipt()?.volume = 0.0
        }
        updateTotal()
        liveReceipt.notifyObserver()
    }
    fun onVolumeChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        volumeText = s.toString()
    }

    var priceText:String = ""
    fun onPriceFocusLost(){
        try {
            getReceipt()?.price = priceText.toDouble()
        } catch (e: NumberFormatException) {
            getReceipt()?.price = 0.0
        }
        updateTotal()
        liveReceipt.notifyObserver()
    }
    fun onPriceChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        priceText = s.toString()
    }

    fun onGasStationUpdated(gasStationId: String?) {
        gasStationId?.let { gasStationId ->
            val source = repository.getGasStation(gasStationId)
            liveGasStation.addSource(source) {
                when (it.status) {
                    DataStatus.SUCCESS -> {
                        liveGasStation.value = it.value
                        liveGasStation.removeSource(source)
                    }
                    DataStatus.ERROR -> {
                        liveGasStation.value = null
                        liveGasStation.removeSource(source)
                    }
                }
            }
        }
    }

    fun onCarUpdated(carId: String?) {
        carId?.let { carId ->
            val source = repository.getCar(carId)
            liveCar.addSource(source) {
                when (it.status) {
                    DataStatus.SUCCESS -> {
                        liveCar.value = it.value
                        liveCar.removeSource(source)
                    }
                    DataStatus.ERROR -> {
                        liveCar.value = null
                        liveCar.removeSource(source)
                    }
                }
            }
        }
    }

    fun onLocationUpdated(location: Location?) {
        location?.let { location ->
            if (getReceipt()?.gasStationId == null) {
                liveGasStation.addSource(geoRepository.getClosestGasStation(location)) { result ->
                    when (result.status) {
                        DataStatus.SUCCESS -> {
                            liveGasStation.value = result.value
                        }
                        DataStatus.ERROR -> {
                            liveGasStation.value = null
                        }
                    }
                }
            }
        }
    }

    fun save(): LiveData<Receipt> {
        onOdometerFocusLost()
        onPriceFocusLost()
        onVolumeFocusLost()

        val liveResult = MediatorLiveData<Receipt>()
        liveGasStation.value?.let {
            getReceipt()?.gasStationId = it.id
            getReceipt()?.gasStationName = it.name
            repository.addGasStation(it)
        }
        liveCar.value?.let {
            getReceipt()?.carId = it.id
            getReceipt()?.carName = it.toString()
            repository.addCar(it)
        }
        getReceipt()?.let {
            liveResult.addSource(repository.addReceipt(it)) { result ->
                when (result.status) {
                    DataStatus.SUCCESS -> {
                        liveResult.value = result.value
                    }
                    DataStatus.ERROR -> {
                        liveResult.value = null
                    }
                }
            }
        } ?: run {
            liveResult.value = null
        }
        return liveResult
    }
}