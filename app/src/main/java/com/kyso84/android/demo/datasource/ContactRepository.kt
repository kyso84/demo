package com.kyso84.android.demo.datasource

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.model.Contact
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.LocalDataOnlyStrategy

interface ContactRepository {
    fun getContactList(): LiveData<DataWrapper<List<Contact>>>
    fun searchContact(text: String): LiveData<DataWrapper<List<Contact>>>
    fun getContactListFromEmailList(emailList: List<String>): LiveData<DataWrapper<List<Contact>>>
}