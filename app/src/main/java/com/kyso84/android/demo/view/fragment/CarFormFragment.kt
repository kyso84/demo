package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.* // ktlint-disable no-wildcard-imports
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentCarFormBinding
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasTypeGroup
import com.kyso84.android.demo.model.car.CarMake
import com.kyso84.android.demo.model.car.CarModel
import javax.inject.Inject
import com.kyso84.android.demo.viewmodel.CarFormViewModel
import com.kyso84.android.demo.viewmodel.OnBoardingViewModel

class CarFormFragment : BaseFragment<FragmentCarFormBinding, CarFormViewModel>(), OnBoardingViewModel.OnBoardingView<CarFormFragment> {

    companion object {
        const val TAG = "CarFormFragment"
    }

    @Inject
    lateinit var router: Router

    override fun getViewModelInstance(): CarFormViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(CarFormViewModel::class.java)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentCarFormBinding>(inflater, com.kyso84.android.demo.R.layout.fragment_car_form, container, false).root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewReady(viewModel: CarFormViewModel, savedInstanceState: Bundle?) {
        viewModel.setCar(arguments?.getParcelable<Car>(Const.KEY_CAR))

        viewModel.yearList.observe(this, Observer { list ->
            binding.fragmentCarFormYear.adapter = ArrayAdapter(this@CarFormFragment.context, android.R.layout.simple_list_item_1, list)
            viewModel.year.value?.let {
                val position = list.indexOf(it)
                binding.fragmentCarFormYear.setSelection(position)
            }
        })
        binding.fragmentCarFormYear.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.year.value = null
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.year.value = binding.fragmentCarFormYear.adapter.getItem(position) as String
            }
        }
        viewModel.makeList.observe(this, Observer {
            binding.fragmentCarFormMake.adapter = ArrayAdapter(this@CarFormFragment.context, android.R.layout.simple_list_item_1, it)
        })
        binding.fragmentCarFormMake.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.make.value = null
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.make.value = binding.fragmentCarFormMake.adapter.getItem(position) as CarMake
            }
        }
        viewModel.modelList.observe(this, Observer {
            binding.fragmentCarFormModel.adapter = ArrayAdapter(this@CarFormFragment.context, android.R.layout.simple_list_item_1, it)
        })
        binding.fragmentCarFormModel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.model.value = null
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.model.value = binding.fragmentCarFormModel.adapter.getItem(position) as CarModel
            }
        }
        binding.fragmentCarFormAlias.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.alias.value = s.toString()
            }
        })
        binding.fragmentCarFormGasType.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.fragment_car_form_gazoline -> viewModel.gasTypeGroup.value = GasTypeGroup.GAZOLINE
                R.id.fragment_car_form_diesel -> viewModel.gasTypeGroup.value = GasTypeGroup.DIESEL
                R.id.fragment_car_form_electric -> viewModel.gasTypeGroup.value = GasTypeGroup.ELECTRIC
            }
        }
        binding.fragmentCarFormSave.setOnClickListener {
            if (viewModel.save()) {
                router.back(binding.root)
            }
        }
    }
}
