package com.kyso84.android.demo.core.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStore
import com.kyso84.android.demo.BR
import com.kyso84.android.demo.injection.AppComponent
import com.kyso84.android.demo.injection.ViewModelFactory
import com.quicklib.android.mvvm.view.QuickViewHolder
import javax.inject.Inject

abstract class BaseViewHolder<T, VDB : ViewDataBinding, VM : ViewModel>(lifecycleOwner: LifecycleOwner, binding: VDB) : QuickViewHolder<T, VDB, VM>(lifecycleOwner = lifecycleOwner, binding = binding) {

    protected lateinit var appComponent: AppComponent
    val viewModelStore
            get() = ViewModelStore()

    @Inject
    lateinit var viewModeFactory: ViewModelFactory

    override fun onViewReady(savedInstanceState: Bundle?) {
        super.onViewReady(savedInstanceState)
        binding.setVariable(BR.viewModel, viewModel)
    }
}