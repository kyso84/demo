package com.kyso84.android.demo.core.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.kyso84.android.demo.BR
import com.kyso84.android.demo.injection.AppComponent
import com.kyso84.android.demo.injection.DaggerFragmentComponent
import com.kyso84.android.demo.injection.FragmentComponent
import com.kyso84.android.demo.injection.ViewModelFactory
import com.quicklib.android.mvvm.view.QuickDialogFragment
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseDialogFragment<VDB : ViewDataBinding, VM : ViewModel> : QuickDialogFragment<VDB, VM>() {

    @Inject
    protected lateinit var appComponent: AppComponent
    protected lateinit var fragmentComponent: FragmentComponent

    @Inject
    lateinit var viewModeFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as HasSupportFragmentInjector).supportFragmentInjector().inject(this)
        fragmentComponent = DaggerFragmentComponent.builder().activityComponent((activity as BaseActivity<*, *>).activityComponent).fragment(this).build()
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        binding.setVariable(BR.viewModel, viewModel)
        onViewReady(viewModel, savedInstanceState)
    }

    abstract fun onViewReady(viewModel: VM, savedInstanceState: Bundle?)
}
