package com.kyso84.android.demo.view.adapter

import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ListAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.core.SimpleItemTouchHelperCallback
import com.kyso84.android.demo.core.base.BaseRecyclerAdapter
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.model.ExportFormat
import kotlinx.android.synthetic.main.part_export_field_row.view.*

class ContactAdapter(private val layoutInflater: LayoutInflater, private val filter:Filter) : BaseRecyclerAdapter<Contact,ExportRecipientViewHolder >() , Filterable{

    override fun getFilter() = filter


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExportRecipientViewHolder = ExportRecipientViewHolder(layoutInflater, parent)


    override fun onBindViewHolder(holder: ExportRecipientViewHolder, position: Int) {
        val contact = getItem(position)
        holder.bind(contact)
        holder.binding.partExportRecipientRowClear.setOnClickListener {
            removeAt(position)
        }
    }


}