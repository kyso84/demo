package com.kyso84.android.demo.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.ABORT
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt

@Dao
interface AppDao {
    // CAR__________________________________________________________________________________________

    @Query("SELECT * FROM car")
    fun watchCarList(): LiveData<List<Car>>

    @Query("SELECT * FROM car")
    fun getCarList(): List<Car>

    @Query("SELECT * FROM car WHERE id = :carId")
    fun getCar(carId: String): Car

    @Insert(onConflict = REPLACE)
    fun insertCar(vararg data: Car)

    @Insert(onConflict = REPLACE)
    fun insertCar(data: List<Car>)

    @Delete
    fun deleteCar(data: Car)

    // RECEIPT______________________________________________________________________________________

    @Query("SELECT * FROM receipt")
    fun watchReceiptList(): LiveData<List<Receipt>>

    @Query("SELECT * FROM receipt")
    fun getReceiptList(): List<Receipt>

    @Query("SELECT * FROM receipt WHERE id = :receiptId")
    fun getReceipt(receiptId: Long): Receipt

    @Insert(onConflict = ABORT)
    fun insertReceipt(vararg data: Receipt): List<Long>

    @Insert(onConflict = ABORT)
    fun insertReceipt(data: List<Receipt>)

    @Delete
    fun deleteReceipt(data: Receipt)

    // GAS STATION__________________________________________________________________________________

    @Query("SELECT * FROM gas_station")
    fun watchGasStationList(): LiveData<List<GasStation>>

    @Query("SELECT * FROM gas_station")
    fun getGasStationList(): List<GasStation>

    @Query("SELECT * FROM gas_station WHERE id = :gasStationId")
    fun getGasStation(gasStationId: String): GasStation

    @Insert(onConflict = REPLACE)
    fun insertGasStation(vararg data: GasStation)

    @Insert(onConflict = REPLACE)
    fun insertGasStation(data: List<GasStation>)

    @Delete
    fun deleteGasStation(data: GasStation)
}