package com.kyso84.android.demo.injection.module

import androidx.lifecycle.ViewModel
import com.kyso84.android.demo.injection.ViewModelKey
import com.kyso84.android.demo.viewmodel.CarFormViewModel
import com.kyso84.android.demo.viewmodel.CarPickerViewModel
import com.kyso84.android.demo.viewmodel.ExportViewModel
import com.kyso84.android.demo.viewmodel.GasStationViewModel
import com.kyso84.android.demo.viewmodel.MainViewModel
import com.kyso84.android.demo.viewmodel.OnBoardingViewModel
import com.kyso84.android.demo.viewmodel.ReceiptFormViewModel
import com.kyso84.android.demo.viewmodel.ReceiptListViewModel
import com.kyso84.android.demo.viewmodel.ReceiptViewModel
import com.kyso84.android.demo.viewmodel.SplashViewModel
import com.kyso84.android.demo.viewmodel.VoidViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(VoidViewModel::class)
    abstract fun bindVoidViewModel(viewModel: VoidViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExportViewModel::class)
    abstract fun bindExportViewModel(exportViewModel: ExportViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReceiptFormViewModel::class)
    abstract fun bindReceiptFormViewModel(viewModel: ReceiptFormViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReceiptListViewModel::class)
    abstract fun bindReceiptListViewModel(viewModel: ReceiptListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReceiptViewModel::class)
    abstract fun bindReceiptViewModel(viewModel: ReceiptViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CarPickerViewModel::class)
    abstract fun bindCarPickerViewModel(viewModel: CarPickerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CarFormViewModel::class)
    abstract fun bindCarFormViewModel(viewModel: CarFormViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnBoardingViewModel::class)
    abstract fun bindOnBoardingViewModel(viewModel: OnBoardingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GasStationViewModel::class)
    abstract fun bindGasStationViewModel(viewModel: GasStationViewModel): ViewModel

}
