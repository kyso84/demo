package com.kyso84.android.demo.viewmodel

import androidx.fragment.app.Fragment
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import javax.inject.Inject

class OnBoardingViewModel @Inject constructor(private val remoteConfig: FirebaseRemoteConfig, private val repository: AppRepository, private val preferenceService: PreferenceService) : BaseViewModel() {

    companion object {
        const val TAG = "OnBoardingViewModel"
    }

    interface OnBoardingView<F : Fragment>
}