package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.datasource.SystemRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.datasource.remote.AppApi
import com.quicklib.android.network.DataWrapper

class SystemManagedRepository(private val api: AppApi,private val preferenceService: PreferenceService) : SystemRepository{

    override fun fetch(): LiveData<DataWrapper<Boolean>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPublishedVersionCode(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isOnBoardingEnabled(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getStartCounter(): Int = preferenceService.startCount

    override fun getSplashScreenDelay(): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}