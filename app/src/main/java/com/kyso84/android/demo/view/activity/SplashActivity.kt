package com.kyso84.android.demo.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.Tasks.await
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseActivity
import com.kyso84.android.demo.databinding.ActivitySplashBinding
import com.kyso84.android.demo.viewmodel.SplashViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    companion object {
        const val TAG = "SplashActivity"
    }

    @Inject
    lateinit var router: Router

    private var navBundle: Bundle? = null
    private val startTimestamp = System.currentTimeMillis()

    override fun getViewModelInstance(): SplashViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(SplashViewModel::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)

        navBundle = intent.extras
    }

    override fun onViewReady(viewModel: SplashViewModel, savedInstanceState: Bundle?) {
        viewModel.state.observe(this, Observer {
            if (it.count > 0) {
                binding.activitySplashMessage.text = resources.getQuantityText(it.messageId, it.count)
            } else {
                binding.activitySplashMessage.setText(it.messageId)
            }
        })

        viewModel.event.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                when (it) {
                    is SplashViewModel.ViewEvent.OnBoardingStart -> router.openOnBoarding(this)
                    is SplashViewModel.ViewEvent.LoginStart -> router.openLogin(this)
                    is SplashViewModel.ViewEvent.CarCreator -> router.openCarCreator(this)
                    is SplashViewModel.ViewEvent.Done -> {
                        binding.activitySplashMessage.setText(R.string.init_done)
                        delayedAction {
                            router.openMain(this, navBundle)
                            viewModel.clear()
                            finish()
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Const.REQUEST_ON_BOARDING -> viewModel.reload()
            Const.REQUEST_LOGIN -> viewModel.reload()
            Const.REQUEST_CAR_CREATOR -> viewModel.reload()
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun delayedAction(action: () -> Unit) {
        val splashScreenDelay = viewModel.getSplashScreenDelay()
        val waitDelay = (startTimestamp + splashScreenDelay) - System.currentTimeMillis()
        if (waitDelay > 0) {
            runBlocking {
                delay(waitDelay)
            }
        }
        action.invoke()
    }

}
