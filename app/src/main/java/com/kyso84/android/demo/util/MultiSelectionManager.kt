package com.kyso84.android.demo.util

import android.util.SparseBooleanArray
import androidx.core.util.forEach

abstract class MultiSelectionManager(private val selectedItems: SparseBooleanArray = SparseBooleanArray()) {
    abstract fun onMultiSelectionEnabled()
    abstract fun onMultiSelectionDisabled()

    fun toggleSelection(position: Int) {
        if (isSelected(position)) {
            selectedItems.delete(position)
        } else {
            selectedItems.put(position, true)
        }
    }

    fun isSelected(position: Int): Boolean = selectedItems.get(position, false)

    fun clearSelections() {
        selectedItems.clear()
    }

    fun getSelectedItemCount(): Int {
        return selectedItems.size()
    }

    fun getSelectedItems(): List<Int> {
        val items = mutableListOf<Int>()
        selectedItems.forEach { key, value ->
            if (value) {
                items.add(key)
            }
        }
        return items
    }

    enum class Status {
        SELECTED,
        UNSELECTED,
        NONE,
    }
}
