package com.kyso84.android.demo.core.service

import android.content.Context
import com.kyso84.android.demo.core.NamedField
import java.lang.reflect.Field

class MappingService(val context: Context, vararg classList: Class<*>) {

    private val mapping = mutableMapOf<Field, String>()
    private val rmapping = mutableMapOf<String, Field>()

    init {
        classList.forEach { clazz ->
            clazz.declaredFields.forEach { field ->
                field.annotations.forEach { annotation ->
                    if (annotation is NamedField) {
                        val aliasId = annotation.aliasId
                        if (aliasId != 0) {
                            mapping[field] = context.getString(aliasId)
                            rmapping[context.getString(aliasId)] = field
                        }
                    }
                }
            }
        }
    }

    fun getClassMappingFields(clazz: Class<*>): List<Field> = mapping.filter { entry: Map.Entry<Field, String> -> entry.key.declaringClass.isAssignableFrom(clazz) }.keys.toList()
    fun getClassMappingAliases(clazz: Class<*>): List<String> = mapping.filter { entry: Map.Entry<Field, String> -> entry.key.declaringClass.isAssignableFrom(clazz) }.values.toList()

    fun getFields(aliases: List<String>): List<Field> {
        val fields = mutableListOf<Field>()
        aliases.forEach { alias ->
            rmapping[alias]?.let {
                fields.add(it)
            }
        }
        return fields
    }

    fun getFieldNames(fields: List<Field>) : List<String>{
        val aliases = mutableListOf<String>()
        fields.forEach { field ->
            mapping[field]?.let {
                aliases.add(it)
            }
        }
        return aliases
    }

}