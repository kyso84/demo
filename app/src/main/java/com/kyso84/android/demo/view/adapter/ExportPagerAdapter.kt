package com.kyso84.android.demo.view.adapter

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.view.fragment.ExportFieldFragment
import com.kyso84.android.demo.view.fragment.ExportFormatFragment
import com.kyso84.android.demo.view.fragment.ExportRecipientFragment
import com.kyso84.android.demo.view.fragment.PagerFragment
import java.util.ArrayList

class ExportPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = listOf<Fragment>(ExportFieldFragment(), ExportFormatFragment(), ExportRecipientFragment())


    fun setArguments(bundle: Bundle?) {
        bundle?.let { bundle ->
            fragments.forEach { fragment ->
                fragment.arguments = bundle
            }
        }
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size


    override fun getPageTitle(position: Int): CharSequence? {
        val fragment = fragments.get(position)
        if (fragment is PagerFragment) {
            return context.getString(fragment.getTitle())
        }
        return fragment.javaClass.simpleName
    }

}