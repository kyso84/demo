package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.core.service.BillingService
import com.kyso84.android.demo.core.service.SessionService
import com.kyso84.android.demo.core.service.SystemService
import com.kyso84.android.demo.datasource.AppRepository
import com.quicklib.android.mvvm.arch.Event
import com.quicklib.android.network.DataStatus
import javax.inject.Inject

class SplashViewModel @Inject constructor(
        private val systemService: SystemService,
        private val sessionService: SessionService,
        private val billingService: BillingService,
        private val appRepository: AppRepository ) : BaseViewModel() {

    companion object {
        const val TAG = "SplashViewModel"
    }

    sealed class ViewEvent {
        class OnBoardingStart : ViewEvent()
        class LoginStart : ViewEvent()
        class CarCreator : ViewEvent()
        class Done : ViewEvent()
    }

    sealed class ViewState(val messageId: Int, val count:Int = 0) {
        class SystemInit(startCounter:Int) : ViewState(R.plurals.init_system, startCounter)
        class Identification : ViewState(R.string.init_identification)
        class BillingStatus : ViewState(R.string.init_billing)
        class UserCars : ViewState(R.string.init_user_cars)
        class Error : ViewState(R.string.init_error)
    }



    private val liveState = MediatorLiveData<ViewState>()
    val state: LiveData<ViewState> = liveState

    private val liveEvent = MutableLiveData<Event<ViewEvent>>()
    val event: LiveData<Event<ViewEvent>> = liveEvent


    private val stateObserver = Observer<ViewState> { viewState ->
        when (viewState) {
            is ViewState.SystemInit -> doSystemInit()
            is ViewState.Identification -> doIdentification()
            is ViewState.BillingStatus -> doBillingStatus()
            is ViewState.UserCars -> doUserCars()
            is ViewState.Error -> doError()
        }
    }

    init {
        liveState.observeForever(stateObserver)
        liveState.value = ViewState.SystemInit(systemService.getStartCounter())
    }

    fun reload() {
        liveState.value?.let{
            liveState.value = it
        }
    }

    fun doSystemInit() {
        val source = systemService.loaded
        liveState.addSource(source){
            if(it){
                liveState.value = ViewState.Identification()
                liveState.removeSource(source)
            }
        }
    }

    fun doIdentification() {
        val source = sessionService.user
        liveState.addSource(source){
            it?.let{user ->
                liveState.value = ViewState.BillingStatus()
                liveState.removeSource(source)
            }?:run{
                if( systemService.isOnboardingEnabled()  ){
                    liveEvent.value = Event<ViewEvent>(ViewEvent.OnBoardingStart())
                }else{
                    liveEvent.value = Event<ViewEvent>(ViewEvent.LoginStart())
                }
            }
        }
    }

    fun doBillingStatus() {
        val source = billingService.loaded
        liveState.addSource(source){
            if(it){
                liveState.value = ViewState.UserCars()
                liveState.removeSource(source)
            }
        }
    }

    fun doUserCars() {
        val source = appRepository.getCarList()
        liveState.addSource(source){result ->
            when(result.status){
                DataStatus.SUCCESS -> {
                    if( !result.value.isNullOrEmpty() ){
                        liveEvent.value = Event<ViewEvent>(ViewEvent.Done())
                    }else if( systemService.isOnboardingEnabled()  ){
                        liveEvent.value = Event<ViewEvent>(ViewEvent.OnBoardingStart())
                    }else{
                        liveEvent.value = Event<ViewEvent>(ViewEvent.CarCreator())
                    }
                    liveState.removeSource(source)
                }
                DataStatus.ERROR -> {
                    liveState.value = ViewState.Error()
                    liveState.removeSource(source)
                }
            }
        }

    }


    fun doError() {

    }

    fun clear(){
        liveState.removeObserver(stateObserver)
    }

    fun getSplashScreenDelay():Long = systemService.getSplashScreenDelay()


}