package com.kyso84.android.demo.datasource.repo

import android.location.Location
import androidx.annotation.WorkerThread
import com.google.maps.GeoApiContext
import com.google.maps.PlacesApi
import com.google.maps.model.* // ktlint-disable no-wildcard-imports
import com.kyso84.android.demo.datasource.GeoRepository
import com.kyso84.android.demo.model.Address
import com.kyso84.android.demo.model.GasStation
import com.quicklib.android.network.strategy.RemoteDataOnlyStrategy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class GeoApiGoogleRepository(private val geoApi: String, private val geoApiContext: GeoApiContext) :GeoRepository {

    override fun getClosestGasStation(location: Location) = object : RemoteDataOnlyStrategy<GasStation?>() {
        override suspend fun fetchData(): Deferred<GasStation?> {
            val list = getGasStationFromLocation(location, 1)
            val first = if (list.isNotEmpty()) list.first() else null
            return CompletableDeferred(first)
        }
    }.asLiveData()

    override fun getClosestGasStationList(location: Location) = object : RemoteDataOnlyStrategy<List<GasStation>>() {
        override suspend fun fetchData(): Deferred<List<GasStation>> = CompletableDeferred(getGasStationFromLocation(location))
    }.asLiveData()

    @WorkerThread
    private fun getGasStationFromLocation(location: Location, limit: Int? = null): List<GasStation> {
        val gasStationList = mutableListOf<GasStation>()
        val placeType = PlaceType.GAS_STATION
        val latLng = LatLng(location.latitude, location.longitude)
        val query = PlacesApi.nearbySearchQuery(geoApiContext, latLng).type(placeType).rankby(RankBy.DISTANCE)
        query.rankby(RankBy.DISTANCE)
        val placeList: Array<PlacesSearchResult>? = query.await().results

        placeList?.let { placeList ->

            val askedList: List<PlacesSearchResult> = limit?.let { limit ->
                placeList.slice(0 until limit)
            } ?: placeList.toList()

            askedList.forEach { it ->
                val place = PlacesApi.placeDetails(geoApiContext, it.placeId).await()

                // Photos
                val placePhotos = arrayListOf<String>()
                place.photos?.forEach {
                    placePhotos.add("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&maxheight=400&key=${this.geoApi}&photoreference=${it.photoReference}")
                }

                // Address
                val address = Address(place.formattedAddress)
                place.addressComponents.forEach {
                    if (it.types.contains(AddressComponentType.STREET_NUMBER)) {
                        address.streetNumber = it.shortName
                    } else if (it.types.contains(AddressComponentType.ROUTE) || it.types.contains(AddressComponentType.STREET_ADDRESS)) {
                        address.streetName = it.shortName
                    } else if (it.types.contains(AddressComponentType.LOCALITY)) {
                        address.city = it.shortName
                    } else if (it.types.contains(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {
                        address.state = it.shortName
                    } else if (it.types.contains(AddressComponentType.COUNTRY)) {
                        address.country = it.shortName
                    } else if (it.types.contains(AddressComponentType.POSTAL_CODE)) {
                        address.zipCode = it.shortName
                    }
                }
                gasStationList.add(GasStation(id = place.placeId, name = place.name, latitude = place.geometry.location.lat, longitude = place.geometry.location.lng, photoUrlList = placePhotos, address = address))
            }
        }
        return gasStationList
    }
}