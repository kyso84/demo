package com.kyso84.android.demo.injection.module

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.gson.GsonBuilder
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.service.SessionService
import com.kyso84.android.demo.datasource.remote.AppApi
import com.kyso84.android.demo.datasource.remote.CarApi
import com.quicklib.android.security.AndroCrypt
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    @Singleton
    @Provides
    @Named(Const.KEY_RESTRICTED)
    fun provideAuthorizedApi(context: Context, androCrypt: AndroCrypt, sessionService: SessionService, okHttpClientBuilder: OkHttpClient.Builder, gsonBuilder: GsonBuilder): AppApi {
        okHttpClientBuilder.addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
            sessionService.getUserId()?.let {
                val authorization = androCrypt.encrypt(sessionService.getUserId())
                Log.d("XXX", "authorization=$authorization")
                newRequest.addHeader("Authorization", Uri.encode(authorization))
            }

            chain.proceed(newRequest.build())
        }
        return provideApi(context, okHttpClientBuilder, gsonBuilder)
    }

    @Singleton
    @Provides
    @Named(Const.KEY_OPEN)
    fun provideApi(context: Context, okHttpClientBuilder: OkHttpClient.Builder, gsonBuilder: GsonBuilder): AppApi {
        val retrofit = Retrofit.Builder()
                .baseUrl(context.getString(R.string.app_api_url))
                .client(okHttpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build()

        return retrofit.create(AppApi::class.java)
    }

    @Singleton
    @Provides
    fun provideCarApi(context: Context, okHttpClientBuilder: OkHttpClient.Builder, gsonBuilder: GsonBuilder): CarApi {
        okHttpClientBuilder.addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                    .build()
            chain.proceed(newRequest)
        }

        val retrofit = Retrofit.Builder()
                .baseUrl(context.getString(R.string.car_api_url))
                .client(okHttpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build()

        return retrofit.create(CarApi::class.java)
    }
}
