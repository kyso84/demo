package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "car")
data class Car @JvmOverloads constructor(
        @PrimaryKey
        @SerializedName("_id")
        val id: String = "",
        val makeId: String = "",
        val makeName: String = "",
        val modelName: String = "",
        val year: String = "",
        val country: String = "",
        val gasTypeGroup: GasTypeGroup,
        val alias: String = "",
        val imageUrl: String = ""
) : Parcelable {
    constructor() : this(id = "", makeId = "", makeName = "", modelName = "", year = "", country = "", gasTypeGroup = GasTypeGroup.GAZOLINE, alias = "", imageUrl = "")
    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readParcelable(GasTypeGroup::class.java.classLoader),
            parcel.readString() ?: "",
            parcel.readString()) {
    }

    override fun toString() = "$makeName $modelName".trim()
    fun getName() = toString()
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(makeId)
        parcel.writeString(makeName)
        parcel.writeString(modelName)
        parcel.writeString(year)
        parcel.writeString(country)
        parcel.writeParcelable(gasTypeGroup, flags)
        parcel.writeString(alias)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Car> {
        override fun createFromParcel(parcel: Parcel): Car {
            return Car(parcel)
        }

        override fun newArray(size: Int): Array<Car?> {
            return arrayOfNulls(size)
        }
    }


    fun addId(carId: String?): Car = Car(carId
            ?: "", makeId, makeName, modelName, year, country, gasTypeGroup, alias, imageUrl)
}