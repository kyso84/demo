package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.SimpleItemTouchHelperCallback
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentExportFieldBinding
import com.kyso84.android.demo.view.adapter.ExportFieldAdapter
import com.kyso84.android.demo.viewmodel.ExportViewModel


class ExportFieldFragment : BaseFragment<FragmentExportFieldBinding, ExportViewModel>(), PagerFragment {
lateinit var adapter: ExportFieldAdapter

    override fun getTitle() = R.string.colummns

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentExportFieldBinding>(inflater, com.kyso84.android.demo.R.layout.fragment_export_field, container, false).root
    }

    override fun onViewReady(viewModel: ExportViewModel, savedInstanceState: Bundle?) {
        adapter = ExportFieldAdapter(layoutInflater)
        adapter.setItems(viewModel.getInitialFields())
        savedInstanceState?.let {
            adapter.setSelectedItems(it.getStringArrayList(Const.KEY_SELECTED_FIELDS).toList())
        }?:run{
            adapter.setSelectedItems(viewModel.getInitialSelectedFields())
        }
        binding.fragmentExportFieldList.layoutManager = LinearLayoutManager(context)
        binding.fragmentExportFieldList.adapter = adapter


        val touchHelperCallback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(adapter)
        val touchHelper = ItemTouchHelper(touchHelperCallback)
        touchHelper.attachToRecyclerView(binding.fragmentExportFieldList)

        adapter.setTouchHelper(touchHelper)
        adapter.setOnSelectionChangedListener(object : ExportFieldAdapter.OnSelectionChangedListener {
            override fun onSelectionChanged(list: List<String>) {
                viewModel.setExportFieldList(list)
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList(Const.KEY_SELECTED_FIELDS, ArrayList(adapter.getSelectedItems()) )
    }

    override fun getViewModelInstance(): ExportViewModel = ViewModelProviders.of(activity!!, this.viewModeFactory).get(ExportViewModel::class.java)

}