package com.kyso84.android.demo.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.core.service.BillingService
import com.kyso84.android.demo.core.service.LocationService
import com.kyso84.android.demo.databinding.FragmentReceiptFormBinding
import com.kyso84.android.demo.model.GasType
import com.kyso84.android.demo.model.GasTypeGroup
import com.kyso84.android.demo.viewmodel.ReceiptFormViewModel
import javax.inject.Inject

class ReceiptFormFragment : BaseFragment<FragmentReceiptFormBinding, ReceiptFormViewModel>(), LocationService.PermissionCallback {

    companion object {
        const val TAG = "ReceiptFormFragment"
    }

    @Inject
    lateinit var locationService: LocationService
    @Inject
    lateinit var billingService: BillingService
    @Inject
    lateinit var router: Router

    override fun getViewModelInstance(): ReceiptFormViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(ReceiptFormViewModel::class.java)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentReceiptFormBinding>(inflater, R.layout.fragment_receipt_form, container, false).root
    }

    override fun onViewReady(viewModel: ReceiptFormViewModel, savedInstanceState: Bundle?) {
        binding.fragmentReceiptFormCarChange.setOnClickListener {
            router.showCarPicker(view = it, successAction = {})
        }
        binding.fragmentReceiptFormGasStationChange.setOnClickListener { view ->
            viewModel.gasStation.value?.let {
                Toast.makeText(this@ReceiptFormFragment.context, "OPEN GAS STATION PICKER", Toast.LENGTH_SHORT).show()
            } ?: run {
                searchGasStation()
            }
        }
        binding.fragmentReceiptFormTimestamp.setOnClickListener {
            router.promptDateTimeDialog(this@ReceiptFormFragment, viewModel.receipt.value)
        }
        binding.fragmentReceiptFormSave.setOnClickListener {
            viewModel.save().observe(this, Observer {
                it?.let {
                    if (!it.id.isNullOrEmpty()) {
                        Toast.makeText(this@ReceiptFormFragment.context, "Saved id=${it.id}", Toast.LENGTH_LONG).show()
                        activity?.onBackPressed()
                    } else {
                        Toast.makeText(this@ReceiptFormFragment.context, "Save has failed", Toast.LENGTH_LONG).show()
                    }
                } ?: run {
                    Toast.makeText(this@ReceiptFormFragment.context, "Save has failed", Toast.LENGTH_LONG).show()
                }
            })
        }
        binding.locationService = locationService
        binding.billingService = billingService
        binding.router = router

        viewModel.car.observe(this, Observer { car ->
            car?.let {
                if (car.gasTypeGroup.gasTypeList.size > 1) {
                    binding.fragmentReceiptFormCar.partCarRowOverflow.setOnClickListener { showGasTypePopup(it, car.gasTypeGroup) }
                    binding.fragmentReceiptFormCar.partCarRowGasType.setOnClickListener { showGasTypePopup(binding.fragmentReceiptFormCar.partCarRowOverflow, car.gasTypeGroup) }
                } else {
                    binding.fragmentReceiptFormCar.partCarRowOverflow.setOnClickListener(null)
                    binding.fragmentReceiptFormCar.partCarRowGasType.setOnClickListener(null)
                }
                binding.fragmentReceiptFormCar.invalidateAll()
            }
        })
        locationService.location.observe(this, Observer {
            viewModel.onLocationUpdated(it)
        })

        viewModel.gasStation.observe(this, Observer {gasStation->
            binding.fragmentReceiptFormGasStation.invalidateAll()
            binding.fragmentReceiptFormGasStation.partGasStationRowImage.setOnClickListener {
                router.showGasStationViewer(it, gasStation)
            }
        })
        binding.fragmentReceiptFormOdometer.partOdometerRowEdittext.setOnFocusChangeListener { v, hasFocus -> if(!hasFocus){viewModel.onOdometerFocusLost()} }
        binding.fragmentReceiptFormPrice.partPriceRowEdittext.setOnFocusChangeListener { v, hasFocus -> if(!hasFocus){viewModel.onPriceFocusLost()} }
        binding.fragmentReceiptFormVolume.partVolumeRowEdittext.setOnFocusChangeListener { v, hasFocus -> if(!hasFocus){viewModel.onVolumeFocusLost()} }
        viewModel.receipt.observe(this, Observer {
            binding.invalidateAll()
        })

        // Load existing receipt
        arguments?.let {
            val args = ReceiptFormFragmentArgs.fromBundle(it)
            args.receipt?.let {
                viewModel.start(oldReceipt = args.receipt)
            } ?: run {
                viewModel.start(car = args.car, gasStation = args.gasStation)
            }
        }

        // First gas station search
        viewModel.receipt.value?.gasStationId?.let { gasStationId ->
            Log.d("XXX", "We already have a gas station")
        } ?: run {
            searchGasStation()
        }
    }

    private fun searchGasStation() {
        locationService.askLocation(this, this)
        binding.fragmentReceiptFormGasStation.partGasStationRowStatus.text = getString(R.string.gas_station_searching)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.REQUEST_DATETIME_PICKER -> if (resultCode == Activity.RESULT_OK) {
                data?.let {
                    viewModel.receipt.value?.timeStamp = it.getLongExtra(Const.KEY_TIMESTAMP, System.currentTimeMillis())
                    binding.invalidateAll()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationService.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun showGasTypePopup(anchor: View, gasTypeGroup: GasTypeGroup) {
        val popup = PopupMenu(context, anchor)
        when(gasTypeGroup){
            GasTypeGroup.ELECTRIC -> popup.menuInflater.inflate(R.menu.popup_gas_group_electric, popup.menu)
            GasTypeGroup.DIESEL -> popup.menuInflater.inflate(R.menu.popup_gas_group_diesel, popup.menu)
            else -> popup.menuInflater.inflate(R.menu.popup_gas_group_gazoline, popup.menu)
        }
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.gastype_regular -> viewModel.onGasTypeChange(GasType.REGULAR)
                R.id.gastype_medium -> viewModel.onGasTypeChange(GasType.MEDIUM)
                R.id.gastype_premium -> viewModel.onGasTypeChange(GasType.PREMIUM)
                R.id.gastype_diesel -> viewModel.onGasTypeChange(GasType.DIESEL)
                R.id.gastype_electric -> viewModel.onGasTypeChange(GasType.ELECTRIC)
            }
            true
        }
        popup.show()
    }

    override fun inciteToGrantPermission() {
        router.promptLocationPermissionDialog(this, this)
    }

    override fun onPermissionDeclined() {
        binding.fragmentReceiptFormGasStation.partGasStationRowStatus.text = getString(R.string.gas_station_unavailable)
    }
}