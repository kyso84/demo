package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.kyso84.android.demo.R
import com.squareup.picasso.Picasso


class ImageCarouselAdapter(private val layoutInflater: LayoutInflater, private val urlList:List<String> = emptyList()) : PagerAdapter(){

    override fun getCount(): Int {
        return urlList.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return o === view
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater.inflate(R.layout.part_image_carousel_tile, container, false)

        val url = urlList[position]
        val image = view.findViewById<ImageView>(R.id.part_image_carousel_tile)
        Picasso.get().load(url).into(image)

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int,view: Any) {
        container.removeView(view as View)
    }
}