package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataStatus
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.FirebaseFirestoreReadListStrategy
import com.quicklib.android.network.strategy.FirebaseFirestoreReadObjectStrategy
import com.quicklib.android.network.strategy.FirebaseFirestoreWriteStrategy

class AppFirebaseFirestoreRepository(private val authRepository: AuthRepository, private val firestoreRoot: FirebaseFirestore) : AppRepository {
    companion object {
        const val ENDPOINT_RECEIPT = "receipts"
        const val ENDPOINT_GAS_STATION = "gas_stations"
        const val ENDPOINT_CARS = "cars"
    }

    private fun getFirestore(): FirebaseFirestore = firestoreRoot // authRepository.getUser()?.let{firestoreRoot.getReference("v1/users/$it")}?:firestoreRoot.getReference("v1/public")


    override fun addReceipt(receipt: Receipt): LiveData<DataWrapper<Receipt>> = object : FirebaseFirestoreWriteStrategy<Receipt>(getFirestore().collection(ENDPOINT_RECEIPT), receipt, { newId -> receipt.addId(newId) }) {}.asLiveData()

    override fun getReceiptList(): LiveData<DataWrapper<List<Receipt>>> = object : FirebaseFirestoreReadListStrategy<Receipt>(getFirestore().collection(ENDPOINT_RECEIPT)) {
        override fun deserialize(snapshot: QuerySnapshot): List<Receipt> = snapshot.toObjects(Receipt::class.java)
    }.asLiveData()

    override fun addGasStation(gasStation: GasStation): LiveData<DataWrapper<GasStation>> = object : FirebaseFirestoreWriteStrategy<GasStation>(getFirestore().collection(ENDPOINT_GAS_STATION), gasStation, { newId -> gasStation.addId(newId) }) {}.asLiveData()

    override fun getGasStation(gasStationId: String): LiveData<DataWrapper<GasStation>> = object : FirebaseFirestoreReadObjectStrategy<GasStation>(getFirestore().collection(ENDPOINT_GAS_STATION).document(gasStationId)) {
        override fun deserialize(snapshot: DocumentSnapshot): GasStation? = snapshot.toObject(GasStation::class.java)
    }.asLiveData()

    override fun getGasStationList(): LiveData<DataWrapper<List<GasStation>>> = object : FirebaseFirestoreReadListStrategy<GasStation>(getFirestore().collection(ENDPOINT_GAS_STATION)) {
        override fun deserialize(snapshot: QuerySnapshot): List<GasStation> = snapshot.toObjects(GasStation::class.java)
    }.asLiveData()

    override fun addCar(car: Car): LiveData<DataWrapper<Car>> = object : FirebaseFirestoreWriteStrategy<Car>(getFirestore().collection(ENDPOINT_CARS), car, { newId -> car.addId(newId) }) {}.asLiveData()

    override fun getCar(carId: String): LiveData<DataWrapper<Car>> = object : FirebaseFirestoreReadObjectStrategy<Car>(getFirestore().collection(ENDPOINT_CARS).document(carId)) {
        override fun deserialize(snapshot: DocumentSnapshot): Car? = snapshot.toObject(Car::class.java)
    }.asLiveData()

    override fun getLatestCar(): LiveData<DataWrapper<Car?>> {
        val result = MutableLiveData<DataWrapper<Car?>>()
        getFirestore().collection(ENDPOINT_CARS).get()
                .addOnSuccessListener { res ->
                    val list: List<Car?> = res.toObjects(Car::class.java)
                    result.postValue(DataWrapper(value = list.first(), status = DataStatus.SUCCESS, localData = true, strategy = null))
                }
                .addOnFailureListener {
                    result.postValue(DataWrapper(error = it, status = DataStatus.ERROR, localData = true, strategy = null))
                }
        return result
    }

    override fun getCarList(): LiveData<DataWrapper<List<Car>>> = object : FirebaseFirestoreReadListStrategy<Car>(getFirestore().collection(ENDPOINT_CARS)) {
        override fun deserialize(snapshot: QuerySnapshot): List<Car> = snapshot.toObjects(Car::class.java)
    }.asLiveData()

}