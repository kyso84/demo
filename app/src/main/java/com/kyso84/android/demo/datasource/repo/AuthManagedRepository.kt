package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.datasource.remote.AppApi
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.model.User
import com.quicklib.android.network.DataWrapper

class AuthManagedRepository(private val appApi: AppApi, private val preferenceService: PreferenceService) : AuthRepository {
    override fun getUser(): User? = preferenceService.user

    override fun loginAnonymously(): LiveData<DataWrapper<User>> {
        TODO("not implemented") //To change body of created functions use File | SystemService | File Templates.
    }

    override fun login(email: String, password: String): LiveData<DataWrapper<User>> {
        TODO("not implemented") //To change body of created functions use File | SystemService | File Templates.
    }

}