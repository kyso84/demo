package com.kyso84.android.demo.core.service

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.model.User
import com.quicklib.android.network.DataStatus
import javax.inject.Singleton

@Singleton
class SessionService(private val authRepository: AuthRepository) {
    private val liveUser = MediatorLiveData<User>()
    val user: LiveData<User> = liveUser

    init {
        authRepository.getUser()?.let {
            liveUser.value = it
        } ?: run {
            liveUser.addSource(authRepository.loginAnonymously()) { result ->
                when (result.status) {
                    DataStatus.SUCCESS -> liveUser.value = result.value
                    else -> {
                    } //TODO
                }
            }
        }
    }


    fun login(user: User) {

    }

    fun isActive() = liveUser.value != null
    fun getUserId(): String? = liveUser.value?.id
}