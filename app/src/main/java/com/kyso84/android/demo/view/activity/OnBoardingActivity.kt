package com.kyso84.android.demo.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseActivity
import com.kyso84.android.demo.databinding.ActivityOnBoardingBinding
import com.kyso84.android.demo.viewmodel.OnBoardingViewModel
import javax.inject.Inject

class OnBoardingActivity : BaseActivity<ActivityOnBoardingBinding, OnBoardingViewModel>() {

    companion object {
        const val TAG = "OnBoardingActivity"
    }

    @Inject
    lateinit var router: Router

    override fun getViewModelInstance(): OnBoardingViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(OnBoardingViewModel::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityOnBoardingBinding>(this, R.layout.activity_on_boarding)
    }

    override fun onViewReady(viewModel: OnBoardingViewModel, savedInstanceState: Bundle?) {
    }
}
