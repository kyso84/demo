package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import com.kyso84.android.demo.App
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseViewHolder
import com.kyso84.android.demo.core.base.BaseViewModelProviders
import com.kyso84.android.demo.databinding.PartReceiptRowBinding
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.util.MultiSelectionManager
import com.kyso84.android.demo.viewmodel.ReceiptViewModel
import javax.inject.Inject

class ReceiptRowViewHolder(lifecycleOwner: LifecycleOwner, layoutInflater: LayoutInflater, container: ViewGroup?, private val multiSelectionManager: MultiSelectionManager? = null) : BaseViewHolder<Receipt, PartReceiptRowBinding, ReceiptViewModel>(lifecycleOwner = lifecycleOwner, binding = PartReceiptRowBinding.inflate(layoutInflater, container, false)) {
//    constructor(activity: AppCompatActivity, container: ViewGroup?) : this(lifecycleOwner = activity, layoutInflater = activity.layoutInflater as LayoutInflater, container = container)
//    constructor(fragment: Fragment, container: ViewGroup?) : this(lifecycleOwner = fragment, layoutInflater = fragment.layoutInflater, container = container)

    @Inject
    lateinit var router: Router

    override fun onBindingReady(binding: PartReceiptRowBinding) {
        super.onBindingReady(binding)
        appComponent = App.get().appComponent
        appComponent.inject(this)
    }

    override fun getViewModelInstance(): ReceiptViewModel = BaseViewModelProviders.of(this, this.viewModeFactory).get(ReceiptViewModel::class.java)

    override fun bind(data: Receipt, position: Int) {
        viewModel.setReceipt(data)
        itemView.setOnClickListener { view ->
            multiSelectionManager?.let {
                if (it.getSelectedItemCount() > 0) {
                    it.toggleSelection(position)
                    viewModel.selected.value = if (it.isSelected(position)) MultiSelectionManager.Status.SELECTED else MultiSelectionManager.Status.UNSELECTED
                    if (it.getSelectedItemCount() == 0) {
                        it.onMultiSelectionDisabled()
                    }
                } else {
                    viewModel.selected.value = MultiSelectionManager.Status.NONE
                    router.showReceiptEditor(view, data)
                }
            } ?: run {
                viewModel.selected.value = MultiSelectionManager.Status.NONE
                router.showReceiptEditor(view, data)
            }
            binding.invalidateAll()
        }
        itemView.setOnLongClickListener { view ->
            multiSelectionManager?.let {
                it.toggleSelection(position)
                viewModel.selected.value = if (it.isSelected(position)) MultiSelectionManager.Status.SELECTED else MultiSelectionManager.Status.UNSELECTED
                if (it.getSelectedItemCount() > 0) {
                    it.onMultiSelectionEnabled()
                } else {
                    it.onMultiSelectionDisabled()
                }
            }
            binding.invalidateAll()
            true
        }
    }
}