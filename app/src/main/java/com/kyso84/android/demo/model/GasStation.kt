package com.kyso84.android.demo.model

import android.location.Location
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "gas_station")
data class GasStation @JvmOverloads constructor(
        @PrimaryKey
        @SerializedName("_id")
        val id: String = "",
        val name: String = "",
        val latitude: Double = 0.0,
        val longitude: Double = 0.0,
        val address: Address? = null,
        val photoUrlList: ArrayList<String> = arrayListOf()
) : Parcelable {
    constructor(parcel: Parcel) : this(
            id = parcel.readString() ?: "",
            name = parcel.readString() ?: "",
            latitude = parcel.readDouble(),
            longitude = parcel.readDouble(),
            address = parcel.readParcelable(Address::class.java.classLoader),
            photoUrlList = parcel.createStringArrayList() ?: arrayListOf()
    )

    fun getLocation(): Location {
        val location = Location("internal")
        location.latitude = latitude
        location.longitude = longitude
        return location
    }

    fun getPhotoUrl(): String {
        if (photoUrlList.isNotEmpty()) {
            return photoUrlList[0]
        }
        return ""
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeParcelable(address, flags)
        parcel.writeStringList(photoUrlList)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun addId(newId: String?): GasStation = GasStation(newId
            ?: "", name, latitude, longitude, address, photoUrlList)

    companion object CREATOR : Parcelable.Creator<GasStation> {
        override fun createFromParcel(parcel: Parcel): GasStation {
            return GasStation(parcel)
        }

        override fun newArray(size: Int): Array<GasStation?> {
            return arrayOfNulls(size)
        }
    }
}