package com.kyso84.android.demo.injection

import androidx.fragment.app.Fragment
import com.kyso84.android.demo.injection.module.FragmentModule
import com.kyso84.android.demo.injection.scope.FragmentScope
import dagger.BindsInstance
import dagger.Component

@FragmentScope
@Component(dependencies = [ActivityComponent::class], modules = [(FragmentModule::class)])
interface FragmentComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun fragment(fragment: Fragment): FragmentComponent.Builder
        fun activityComponent(activityComponent: ActivityComponent): FragmentComponent.Builder
        fun build(): FragmentComponent
    }
}
