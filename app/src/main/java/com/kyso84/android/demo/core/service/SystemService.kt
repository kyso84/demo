package com.kyso84.android.demo.core.service

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.kyso84.android.demo.datasource.SystemRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.quicklib.android.network.DataStatus

class SystemService(private val systemRepository: SystemRepository,private val  preferenceService: PreferenceService) {
    private val liveLoaded = MediatorLiveData<Boolean>()
    val loaded: LiveData<Boolean> = liveLoaded

    init {
        preferenceService.startCount = preferenceService.startCount + 1
        liveLoaded.addSource(systemRepository.fetch()) { result ->
            when (result.status) {
                DataStatus.SUCCESS -> liveLoaded.value = true
                else -> {
                } //TODO
            }
        }
    }

    fun getStartCounter():Int = systemRepository.getStartCounter()
    fun isOnboardingEnabled():Boolean = systemRepository.isOnBoardingEnabled()
    fun getSplashScreenDelay(): Long = systemRepository.getSplashScreenDelay()

}