package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.core.base.BaseViewHolder
import com.kyso84.android.demo.databinding.PartExportFieldRowBinding
import com.kyso84.android.demo.databinding.PartReceiptRowBinding
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.util.MultiSelectionManager
import com.kyso84.android.demo.viewmodel.ReceiptViewModel
import java.lang.reflect.Field

class ExportFieldViewHolder(val binding : PartExportFieldRowBinding  ) :RecyclerView.ViewHolder( binding.root) {
    constructor(layoutInflater: LayoutInflater, container: ViewGroup?):this(PartExportFieldRowBinding.inflate(layoutInflater, container, false))


    fun bind(field: String, selected: Boolean?) {
        binding.partExportFieldRowTitle.text = field
        selected?.let{
            binding.partExportFieldRowTitle.isSelected = it
            binding.partExportFieldRowDragHolder.isSelected = it
        }
    }
}