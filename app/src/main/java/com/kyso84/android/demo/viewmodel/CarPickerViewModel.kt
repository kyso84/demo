package com.kyso84.android.demo.viewmodel

import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import javax.inject.Inject

class CarPickerViewModel @Inject constructor(private val repository: AppRepository, private val preferenceService: PreferenceService) : BaseViewModel()