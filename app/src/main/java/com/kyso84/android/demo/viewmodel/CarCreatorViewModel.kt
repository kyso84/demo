package com.kyso84.android.demo.viewmodel

import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.CarRepository
import javax.inject.Inject

class CarCreatorViewModel @Inject constructor(private val carRepository: CarRepository) : BaseViewModel() {

    companion object {
        const val TAG = "CarFormViewModel"
    }
}