package com.kyso84.android.demo.injection.module

import com.kyso84.android.demo.model.Export
import dagger.Module
import dagger.Provides

@Module
class ActivityModule{

    @Provides
    fun provideExport() = Export()

}
