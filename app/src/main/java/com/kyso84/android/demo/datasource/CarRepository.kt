package com.kyso84.android.demo.datasource

import com.kyso84.android.demo.datasource.remote.CarApi
import com.kyso84.android.demo.model.car.CarMake
import com.kyso84.android.demo.model.car.CarModel
import com.quicklib.android.network.strategy.RemoteDataOnlyStrategy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class CarRepository(private val carApi: CarApi) {

    fun getMakeList(year: String) = object : RemoteDataOnlyStrategy<List<CarMake>>() {
        override suspend fun fetchData(): Deferred<List<CarMake>> = CompletableDeferred(carApi.getMakes(year).execute().body()?.Makes
                ?: listOf())
    }.asLiveData()

    fun getModelList(year: String, carMake: CarMake) = object : RemoteDataOnlyStrategy<List<CarModel>>() {
        override suspend fun fetchData(): Deferred<List<CarModel>> = CompletableDeferred(carApi.getModels(carMake.id, year).execute().body()?.Models
                ?: listOf())
    }.asLiveData()
}