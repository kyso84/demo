package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable

class OnBoardingConf(
    val title: String
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OnBoardingConf> {
        override fun createFromParcel(parcel: Parcel): OnBoardingConf {
            return OnBoardingConf(parcel)
        }

        override fun newArray(size: Int): Array<OnBoardingConf?> {
            return arrayOfNulls(size)
        }
    }
}