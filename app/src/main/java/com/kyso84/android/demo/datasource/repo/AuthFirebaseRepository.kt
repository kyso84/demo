package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.model.User
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.SynchronousStrategy

class AuthFirebaseRepository(private val firebaseAuth: FirebaseAuth) : AuthRepository {


    override fun getUser(): User? = firebaseAuth.currentUser?.let { User(it) }

    override fun loginAnonymously() = object : SynchronousStrategy<User>() {
        override fun getData(): LiveData<User> {
            val result = MutableLiveData<User>()
            firebaseAuth.signInAnonymously().addOnCompleteListener { task ->
                firebaseAuth.currentUser?.let { firebaseUser ->
                    task.exception?.let { exception ->
                        throw exception
                    } ?: run {
                        result.value = User(firebaseUser)
                    }
                } ?: run {
                    throw Exception("An error has occurred")
                }
            }
            return result
        }
    }.asLiveData()

    override fun login(email: String, password: String): LiveData<DataWrapper<User>> {
        TODO("not implemented") //To change body of created functions use File | SystemService | File Templates.
    }


}