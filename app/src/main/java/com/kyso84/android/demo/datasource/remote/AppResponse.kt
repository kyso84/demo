package com.kyso84.android.demo.datasource.remote

import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

 class AppResponse<T>(
        val method: String = "",
        val data: T? = null,
        val errorCode: Int = 0,
        val errorMessage: String = "") {

    val success = data != null && errorCode == 0

    @Throws(ApiException::class)
    fun getResult(): T {
        data?.let {
            return it
        } ?: run {
            throw ApiException(this)
        }
    }
}