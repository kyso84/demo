package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.core.SimpleItemTouchHelperCallback
import com.kyso84.android.demo.model.ExportFormat
import kotlinx.android.synthetic.main.part_export_field_row.view.*

class ExportFormatAdapter(private val layoutInflater: LayoutInflater, private var selectedItem: ExportFormat? = null) : RecyclerView.Adapter<ExportFormatViewHolder>(){

    private var listener: OnSelectionChangedListener? = null

    interface OnSelectionChangedListener {
        fun onSelectionChanged(exportFormat: ExportFormat)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExportFormatViewHolder = ExportFormatViewHolder(layoutInflater, parent)

    override fun getItemCount(): Int = ExportFormat.values().size


    fun getSelectedItem() = selectedItem

    override fun onBindViewHolder(holder: ExportFormatViewHolder, position: Int) {
        val format = ExportFormat.values()[position]

        holder.bind(format, format == selectedItem)
        holder.binding.root.setOnClickListener {
            selectedItem = format
            listener?.onSelectionChanged(format)
        }
    }

    fun setOnSelectionChangedListener(listener: OnSelectionChangedListener) {
        this.listener = listener
    }
}