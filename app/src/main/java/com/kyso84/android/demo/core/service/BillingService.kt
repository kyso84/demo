package com.kyso84.android.demo.core.service

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.model.Pack

class BillingService {
    private val liveLoaded = MutableLiveData<Boolean>()
    val loaded: LiveData<Boolean> = liveLoaded


    private val liveCarPackPurchased = MutableLiveData<Boolean>()
    val carPackPurchased: LiveData<Boolean> = liveCarPackPurchased

    private val liveExportPackPurchased = MutableLiveData<Boolean>()
    val exportPackPurchased: LiveData<Boolean> = liveExportPackPurchased

    fun isCarPackPurchased() = carPackPurchased.value ?: true
    fun isExportPackPurchased() = carPackPurchased.value ?: true
    fun getCarPackPrice() = "0.99$"

    init {
        liveLoaded.value = true
    }

    fun isPackPurchased(pack: Pack): Boolean {
        return when (pack) {
            Pack.CAR_PACK -> isCarPackPurchased()
            Pack.EXPORT_PACK -> isExportPackPurchased()
            else -> false
        }
    }
}