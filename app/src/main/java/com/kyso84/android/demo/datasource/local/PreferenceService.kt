package com.kyso84.android.demo.datasource.local

import android.content.SharedPreferences
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.model.DistanceUnit
import com.kyso84.android.demo.model.ExportFormat
import com.kyso84.android.demo.model.GasType
import com.kyso84.android.demo.model.User
import com.kyso84.android.demo.model.VolumeUnit
import java.util.*

class PreferenceService(private val sharedPreferences: SharedPreferences,private val gson: Gson) {

    var currency: Currency
        get() = Currency.getInstance(sharedPreferences.getString(Const.KEY_CURRENCY, "USD"))
        set(value) = sharedPreferences.edit().putString(Const.KEY_CURRENCY, value.currencyCode).apply()

    var volumeUnit: VolumeUnit
        get() = VolumeUnit.values()[sharedPreferences.getInt(Const.KEY_VOLUME_UNIT, 0)]
        set(value) = sharedPreferences.edit().putInt(Const.KEY_VOLUME_UNIT, value.ordinal).apply()

    var distanceUnit: DistanceUnit
        get() = DistanceUnit.values()[sharedPreferences.getInt(Const.KEY_DISTANCE_UNIT, 0)]
        set(value) = sharedPreferences.edit().putInt(Const.KEY_DISTANCE_UNIT, value.ordinal).apply()

    var lastGasType: GasType
        get() = GasType.values()[sharedPreferences.getInt(Const.KEY_LAST_GAS_TYPE, 0)]
        set(value) = sharedPreferences.edit().putInt(Const.KEY_LAST_GAS_TYPE, value.ordinal).apply()

    var isSyncEnabled: Boolean
        get() = sharedPreferences.getBoolean(Const.KEY_SYNC_ENABLED, true)
        set(value) = sharedPreferences.edit().putBoolean(Const.KEY_SYNC_ENABLED, value).apply()

    var isFirestore: Boolean
        get() = sharedPreferences.getBoolean(Const.KEY_FIRESTORE, false)
        set(value) = sharedPreferences.edit().putBoolean(Const.KEY_FIRESTORE, value).apply()

    var isLocationRefused: Boolean
        get() = sharedPreferences.getBoolean(Const.KEY_LOCATION_REFUSED, false)
        set(value) = sharedPreferences.edit().putBoolean(Const.KEY_LOCATION_REFUSED, value).apply()

    var lastExportFormat: ExportFormat
        get() = ExportFormat.values()[sharedPreferences.getInt(Const.KEY_LAST_EXPORT_FORMAT, 0)]
        set(value) = sharedPreferences.edit().putInt(Const.KEY_LAST_EXPORT_FORMAT, value.ordinal).apply()

    var lastExportFields: List<String>
        get() = sharedPreferences.getStringSet(Const.KEY_LAST_EXPORT_FIELDS, setOf("Date", "Car name", "Gas Station", "Gas type")).toList()
        set(value) = sharedPreferences.edit().putStringSet(Const.KEY_LAST_EXPORT_FIELDS,value.toSet()).apply()

    var lastExportRecipients: List<Contact>
        get() = gson.fromJson(sharedPreferences.getString(Const.KEY_LAST_EXPORT_RECIPIENTS, "[]"))
        set(value) = sharedPreferences.edit().putString(Const.KEY_LAST_EXPORT_RECIPIENTS, gson.toJson(value)).apply()

    var lastCarId: String
        get() = sharedPreferences.getString(Const.KEY_LAST_CAR_ID, "")
        set(value) = sharedPreferences.edit().putString(Const.KEY_LAST_CAR_ID, value).apply()

    var startCount: Int
        get() = sharedPreferences.getInt(Const.KEY_START_COUNT, 0)
        set(value) = sharedPreferences.edit().putInt(Const.KEY_START_COUNT, value).apply()

    var onBoardingShown: Boolean
        get() = sharedPreferences.getBoolean(Const.KEY_ON_BOARDING_SHOWN, false)
        set(value) = sharedPreferences.edit().putBoolean(Const.KEY_ON_BOARDING_SHOWN, value).apply()

    var user: User?
        get() = gson.fromJson(sharedPreferences.getString(Const.KEY_USER, ""), User::class.java)
        set(value) = sharedPreferences.edit().putString(Const.KEY_USER, gson.toJson(value)).apply()


}