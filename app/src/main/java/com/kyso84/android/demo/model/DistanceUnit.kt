package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class DistanceUnit(@StringRes val alias: Int) : Parcelable {

    KILOMETER(R.string.kilometer),
    MILES(R.string.miles);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DistanceUnit> {
        override fun createFromParcel(parcel: Parcel): DistanceUnit {
            return DistanceUnit.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<DistanceUnit?> {
            return arrayOfNulls(size)
        }
    }
}