package com.kyso84.android.demo.core

import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.location.Location
import android.net.Uri
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.BindingAdapter
import com.kyso84.android.demo.model.DistanceUnit
import com.kyso84.android.demo.model.GasType
import com.kyso84.android.demo.model.VolumeUnit
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.text.ParseException
import java.util.Currency
import java.util.Locale

object BindingAdapters {

    const val TAG = "BindingAdapters"

    @JvmStatic
    @BindingAdapter("selectedIf")
    fun selectedIf(view: View, visible: Boolean?) {
        view.isSelected = visible ?: false
    }

    @JvmStatic
    @BindingAdapter("enableIf")
    fun enableIf(view: View, value: Boolean?) {
        view.isEnabled = value ?: false
    }

    @JvmStatic
    @BindingAdapter("goneIf")
    fun goneIf(view: View, value: Boolean?) {
        view.visibility = if (value == true) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("goneIf")
    fun goneIf(view: View, text: String) {
        view.visibility = if (text.isNullOrEmpty()) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("goneIf")
    fun goneIf(view: View, value: Any?) {
        view.visibility = if (value == null) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("invisibleIf")
    fun invisibleIf(view: View, value: Boolean?) {
        view.visibility = if (value == true) View.INVISIBLE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("invisibleIf")
    fun invisibleIf(view: View, text: String) {
        view.visibility = if (text.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter(value = ["srcNetwork", "fallback"], requireAll = false)
    fun srcNetwork(imageView: ImageView, url: String?, fallback: Drawable?) {
        if (!url.isNullOrEmpty()) {
            Picasso.get().load(url).into(imageView, object : Callback {
                override fun onSuccess() {}

                override fun onError(e: Exception?) {
                    Log.e(TAG, "srcNetwork: Unable to load image url $url (id=${if (imageView.id != -1) imageView.resources.getResourceEntryName(imageView.id) else -1})", e)
                    fallback?.let {
                        imageView.setImageDrawable(it)
                    }
                }
            })
        } else {
            Log.w(TAG, "srcNetwork: URL is empty (id=${if (imageView.id != -1) imageView.resources.getResourceEntryName(imageView.id) else -1}))")
            fallback?.let {
                imageView.setImageDrawable(it)
            }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["latitude", "longitude", "zoom", "apiKey"], requireAll = true)
    fun srcMap(imageView: ImageView, latitude: Double, longitude: Double, zoom: Int, apiKey: String) {
        val uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap").buildUpon()
        uri.appendQueryParameter("zoom", zoom.toString())
        uri.appendQueryParameter("center", "$latitude,$longitude")
        uri.appendQueryParameter("key", apiKey)
        srcNetwork(imageView, uri.toString(), null)
    }


    @JvmStatic
    @BindingAdapter("text")
    fun text(textView: TextView, value: String) {
        val text = try {
            val number = NumberFormat.getInstance().parse(value)
            if (number.toDouble() == 0.0) {
                ""
            } else {
                value
            }
        } catch (e: ParseException) {
            value
        }
        if (textView.text != text) {
            textView.text = text
        }
    }

//
//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(textView: TextView, value: Double) {
//        val text = value.toStringNonZero()
//        if (textView.text != text) {
//            textView.text = text
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(textView: TextView, value: Int) {
//        val text = value.toStringNonZero()
//        if (textView.text != text) {
//            textView.text = text
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(textView: TextView, value: Long) {
//        val text = value.toStringNonZero()
//        if (textView.text != text) {
//            textView.text = text
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(textView: TextView, value: Float) {
//        val text = value.toStringNonZero()
//        if (textView.text != text) {
//            textView.text = text
//        }
//    }

//    @JvmStatic
//    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
//    fun getText(editText: EditText): String {
//        return editText.text.toString()
//    }

//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(editText: EditText, value: String) {
//        val text = try {
//            val number = NumberFormat.getInstance().parse(value)
//            if (number.toDouble() == 0.0){
//                ""
//            }else{
//                value
//            }
//        } catch (e: ParseException) {
//            value
//        }
//        if (editText.text.toString() != text) {
//            editText.text = SpannableStringBuilder(text)
//        }
//    }

//    @JvmStatic
//    @InverseBindingAdapter(attribute = "text")
//    fun textToDouble(editText: EditText): Double {
//        return editText.text.toString().toDouble()
//    }

//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(editText: EditText, value: Double) {
//        val text = value.toStringNonZero()
//        if (editText.text.toString() != text) {
//            editText.text = SpannableStringBuilder(text)
//        }
//    }

//    @JvmStatic
//    @InverseBindingAdapter(attribute = "text")
//    fun textToInt(editText: EditText): Int {
//        return editText.text.toString().toInt()
//    }

//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(editText: EditText, value: Int) {
//        val text = value.toStringNonZero()
//        if (editText.text.toString() != text) {
//            editText.text = SpannableStringBuilder(text)
//        }
//    }

//    @JvmStatic
//    @InverseBindingAdapter(attribute = "text")
//    fun textToLong(editText: EditText): Long {
//        return editText.text.toString().toLong()
//    }

//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(editText: EditText, value: Long) {
//        val text = value.toStringNonZero()
//        if (editText.text.toString() != text) {
//            editText.text = SpannableStringBuilder(text)
//        }
//    }

//    @JvmStatic
//    @InverseBindingAdapter(attribute = "text")
//    fun textToFloat(editText: EditText): Float {
//        return editText.text.toString().toFloat()
//    }

//    @JvmStatic
//    @BindingAdapter("text")
//    fun text(editText: EditText, value: Float) {
//        val text = value.toStringNonZero()
//        if (editText.text.toString() != text) {
//            editText.text = SpannableStringBuilder(text)
//        }
//    }

    @JvmStatic
    @BindingAdapter("elapsed")
    fun elapsed(textView: TextView, value: Long) {
        val text = DateUtils.getRelativeTimeSpanString(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("date")
    fun date(textView: TextView, value: Long) {
        val text = DateUtils.formatDateTime(textView.context, value, DateUtils.FORMAT_SHOW_TIME or DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_SHOW_YEAR or DateUtils.FORMAT_SHOW_WEEKDAY or DateUtils.FORMAT_ABBREV_ALL)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("percent")
    fun percent(textView: TextView, value: Double) {
        val text = NumberFormat.getPercentInstance(Locale.getDefault()).format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("percent")
    fun percent(textView: TextView, value: Int) {
        val text = NumberFormat.getPercentInstance(Locale.getDefault()).format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("percent")
    fun percent(textView: TextView, value: Long) {
        val text = NumberFormat.getPercentInstance(Locale.getDefault()).format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("percent")
    fun percent(textView: TextView, value: Float) {
        val text = NumberFormat.getPercentInstance(Locale.getDefault()).format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["amount", "currency"], requireAll = true)
    fun amount(textView: TextView, value: Double, currency: Currency) {
        val nf = NumberFormat.getCurrencyInstance(Locale.getDefault())
        nf.currency = currency
        val text = nf.format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["amount", "currency"], requireAll = true)
    fun amount(textView: TextView, value: Int, currency: Currency) {
        val nf = NumberFormat.getCurrencyInstance(Locale.getDefault())
        nf.currency = currency
        val text = nf.format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["amount", "currency"], requireAll = true)
    fun amount(textView: TextView, value: Long, currency: Currency) {
        val nf = NumberFormat.getCurrencyInstance(Locale.getDefault())
        nf.currency = currency
        val text = nf.format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["amount", "currency"], requireAll = true)
    fun amount(textView: TextView, value: Float, currency: Currency) {
        val nf = NumberFormat.getCurrencyInstance(Locale.getDefault())
        nf.currency = currency
        val text = nf.format(value)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["price", "currency", "unit"], requireAll = true)
    fun price(textView: TextView, value: Double, currency: Currency, unit: VolumeUnit) {
        val context = textView.context
        val nf = NumberFormat.getNumberInstance(Locale.getDefault())
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        val text = String.format("%s %s/%s", nf.format(value), currency.symbol, context.getString(unit.alias))
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["price", "currency", "unit"], requireAll = true)
    fun price(textView: TextView, value: Int, currency: Currency, unit: VolumeUnit) {
        val context = textView.context
        val nf = NumberFormat.getNumberInstance(Locale.getDefault())
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        val text = String.format("%s %s/%s", nf.format(value), currency.symbol, context.getString(unit.alias))
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["price", "currency", "unit"], requireAll = true)
    fun price(textView: TextView, value: Long, currency: Currency, unit: VolumeUnit) {
        val context = textView.context
        val nf = NumberFormat.getNumberInstance(Locale.getDefault())
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        val text = String.format("%s %s/%s", nf.format(value), currency.symbol, context.getString(unit.alias))
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["price", "currency", "unit"], requireAll = true)
    fun price(textView: TextView, value: Float, currency: Currency, unit: VolumeUnit) {
        val context = textView.context
        val nf = NumberFormat.getNumberInstance(Locale.getDefault())
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        val text = String.format("%s %s/%s", nf.format(value), currency.symbol, context.getString(unit.alias))
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["currency", "unit"], requireAll = true)
    fun priceUnit(textView: TextView, currency: Currency, unit: VolumeUnit) {
        val context = textView.context
        val text = String.format("%s/%s", currency.symbol, context.getString(unit.alias))
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["currency"], requireAll = true)
    fun priceUnit(textView: TextView, currency: Currency) {
        val text = currency.symbol
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["loc1", "loc2", "unit"])
    fun distance(textView: TextView, loc1: Location?, loc2: Location?, distanceUnit: DistanceUnit?) {
        loc1?.let { loc1 ->
            loc2?.let { loc2 ->
                distanceUnit?.let { distanceUnit ->
                    val distance: Float = if (distanceUnit == DistanceUnit.MILES) loc1.distanceTo(loc2) * 1609.344f else loc1.distanceTo(loc2)
                    val nf = NumberFormat.getNumberInstance(Locale.getDefault())
                    val context = textView.context
                    nf.maximumFractionDigits = 2
                    if (distanceUnit == DistanceUnit.MILES) {
                        textView.text = String.format("%s %s", nf.format(distance), context.getString(distanceUnit.alias))
                    } else if (distance < 1000) {
                        textView.text = String.format("%s m", Math.round(distance))
                    } else {
                        textView.text = String.format("%s km", nf.format(distance / 1000))
                    }
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["altText"], requireAll = true)
    fun altText(textView: TextView, @StringRes textId: Int) {
        val text = textView.context.getText(textId)
        if (textView.text != text) {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["altText"], requireAll = true)
    fun altText(textView: TextView, text: String) {
        if (textView.text != text) {
            textView.text = text
        }
    }

    private fun tintBackground(view: View, @ColorInt color: Int) {
        val background = view.background
        when (background) {
            is ShapeDrawable -> {
                background.paint.color = color
                view.background = background
            }
            is GradientDrawable -> {
                background.setColor(color)
                background.invalidateSelf()
                view.background = background
            }
            else -> view.setBackgroundColor(color)
        }
    }


    @JvmStatic
    @BindingAdapter(value = ["tint"], requireAll = true)
    fun tint(imageView: ImageView, gasType: GasType) {
        val context = imageView.context
        val drawable = DrawableCompat.wrap(imageView.drawable)
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, gasType.foregroundColorId))
        tintBackground(imageView, ContextCompat.getColor(context, gasType.backgroundColorId))
    }

    @JvmStatic
    @BindingAdapter(value = ["tint"], requireAll = true)
    fun tint(textView: TextView, gasType: GasType) {
        val context = textView.context
        textView.setTextColor(ContextCompat.getColor(context, gasType.foregroundColorId))
        tintBackground(textView, ContextCompat.getColor(context, gasType.backgroundColorId))
    }
}
