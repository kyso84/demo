package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class GasType(@StringRes val title: Int, @ColorRes val backgroundColorId: Int, @ColorRes val foregroundColorId: Int, vararg val volumeUnitList: VolumeUnit) : Parcelable {

    REGULAR(R.string.gastype_regular, R.color.gastype_regular_background, R.color.gastype_regular_foreground, VolumeUnit.LITER, VolumeUnit.GALLONS_US, VolumeUnit.GALLONS_GB),
    MEDIUM(R.string.gastype_medium, R.color.gastype_medium_background, R.color.gastype_medium_foreground, VolumeUnit.LITER, VolumeUnit.GALLONS_US, VolumeUnit.GALLONS_GB),
    PREMIUM(R.string.gastype_premium, R.color.gastype_premium_background, R.color.gastype_premium_foreground, VolumeUnit.LITER, VolumeUnit.GALLONS_US, VolumeUnit.GALLONS_GB),
    DIESEL(R.string.gastype_diesel, R.color.gastype_diesel_background, R.color.gastype_diesel_foreground, VolumeUnit.LITER, VolumeUnit.GALLONS_US, VolumeUnit.GALLONS_GB),
    ELECTRIC(R.string.gastype_electric, R.color.gastype_electric_background, R.color.gastype_electric_foreground, VolumeUnit.KW);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GasType> {
        override fun createFromParcel(parcel: Parcel): GasType {
            return GasType.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<GasType?> {
            return arrayOfNulls(size)
        }
    }

    fun getGroup(): GasTypeGroup = GasTypeGroup.values().toList().filter { it.gasTypeList.contains(this) }.first()
}