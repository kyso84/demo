package com.kyso84.android.demo.datasource

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.model.User
import com.quicklib.android.network.DataWrapper

interface AuthRepository{
    fun getUser():User?
    fun loginAnonymously(): LiveData<DataWrapper<User>>
    fun login(email:String, password:String): LiveData<DataWrapper<User>>
}