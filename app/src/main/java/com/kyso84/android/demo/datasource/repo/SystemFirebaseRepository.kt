package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.datasource.SystemRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.SynchronousStrategy
import timber.log.Timber

class SystemFirebaseRepository(private val firebaseRemoteConfig: FirebaseRemoteConfig, private val preferenceService: PreferenceService) : SystemRepository {


    override fun fetch(): LiveData<DataWrapper<Boolean>> = object : SynchronousStrategy<Boolean>() {
        override fun getData(): LiveData<Boolean> {
            val result = MutableLiveData<Boolean>()
            firebaseRemoteConfig.fetch().addOnCompleteListener {
                if (it.isSuccessful) {
                    firebaseRemoteConfig.activate()
                            .addOnSuccessListener {status ->
                                if(status){
                                    Timber.d("Remote config loaded :)")
                                }else{
                                    Timber.w( "Remote config activation has failed")
                                }
                            }.addOnFailureListener {e->
                                Timber.w(e, "Remote config activation has failed")
                            }
                } else {
                    Timber.w(it.exception, "Remote config has failed")
                }
                result.value = true
            }
            return result
        }
    }.asLiveData()

    override fun getPublishedVersionCode(): Int = firebaseRemoteConfig.getLong(Const.KEY_PUBLISHED_VERSION_CODE).toInt()

    override fun isOnBoardingEnabled(): Boolean = firebaseRemoteConfig.getBoolean(Const.CONF_ENABLE_ON_BOARDING)

    override fun getStartCounter(): Int = preferenceService.startCount

    override fun getSplashScreenDelay(): Long = firebaseRemoteConfig.getLong(Const.CONF_SPLASH_SCREEN_DELAY)

}