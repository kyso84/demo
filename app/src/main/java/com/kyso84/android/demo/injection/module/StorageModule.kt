package com.kyso84.android.demo.injection.module

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.google.gson.Gson
import com.kyso84.android.demo.datasource.local.AppDao
import com.kyso84.android.demo.datasource.local.AppDatabase
import com.kyso84.android.demo.datasource.local.PreferenceService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {
    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun providePreferenceService(sharedPreferences: SharedPreferences, gson: Gson): PreferenceService = PreferenceService(sharedPreferences, gson)

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, "database").build()

    @Provides
    @Singleton
    fun provideDao(appDatabase: AppDatabase): AppDao = appDatabase.getAppDao()
}