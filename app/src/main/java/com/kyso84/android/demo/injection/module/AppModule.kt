package com.kyso84.android.demo.injection.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonSerializer
import com.google.maps.GeoApiContext
import com.kyso84.android.demo.BuildConfig
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.service.BillingService
import com.kyso84.android.demo.core.service.LocationService
import com.kyso84.android.demo.core.service.MappingService
import com.kyso84.android.demo.core.service.SessionService
import com.kyso84.android.demo.core.service.SystemService
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.datasource.SystemRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.security.AndroCrypt
import dagger.Module
import dagger.Provides
import java.util.TimeZone
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application.baseContext

    @Provides
    @Singleton
    fun provideGsonBuilder(): GsonBuilder = GsonBuilder()
            .registerTypeAdapter(TimeZone::class.java, JsonDeserializer<TimeZone> { json, typeOfT, context -> TimeZone.getTimeZone(json.toString()) })
            .registerTypeAdapter(TimeZone::class.java, JsonSerializer<TimeZone> { src, typeOfSrc, context ->
                context.serialize(src.id)
            })

    @Provides
    @Singleton
    fun provideGson(gsonBuilder: GsonBuilder): Gson = gsonBuilder.create()

    @Provides
    @Singleton
    fun getLocationService(context: Context) = LocationService(context)

    @Provides
    @Singleton
    fun provideRouter(preferenceService: PreferenceService, billingService: BillingService, locationService: LocationService): Router = Router(preferenceService, billingService, locationService)

    @Provides
    @Singleton
    fun provideSystem(systemRepository: SystemRepository, preferenceService: PreferenceService) = SystemService(systemRepository, preferenceService)

    @Provides
    @Singleton
    fun provideSession(authRepository: AuthRepository) = SessionService(authRepository)

    @Provides
    @Singleton
    fun provideAndroCrypt(): AndroCrypt = AndroCrypt.Builder("Ee4FMMF8L5fmS4FQ", "xy9r8ANwFFgHK49R").setAlgo(AndroCrypt.Algo.AES128).setDebug(BuildConfig.DEBUG).create()

    @Provides
    @Singleton
    fun provideMappingService(context: Context) = MappingService(context, Receipt::class.java)

    @Provides
    @Named(Const.KEY_MAPS_API_KEY)
    @Singleton
    fun provideMapApiKey(context: Context): String = context.getString(R.string.maps_api_key)

    @Provides
    @Named(Const.KEY_API)
    @Singleton
    fun provideGeoApiKey(context: Context): String = context.getString(R.string.geo_api_key)

    @Provides
    @Singleton
    fun provideGoogleGeoApiContext(context: Context, @Named(Const.KEY_API) apiKey: String) = GeoApiContext.Builder()
            .apiKey(apiKey)
            .build()

}
