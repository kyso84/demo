package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.kyso84.android.demo.core.service.BillingService
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.core.service.MappingService
import com.kyso84.android.demo.datasource.ContactRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.model.Export
import com.kyso84.android.demo.model.ExportFormat
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataStatus
import javax.inject.Inject

class ExportViewModel @Inject constructor(private val contactRepository : ContactRepository, private val preferenceService: PreferenceService, private val mappingService: MappingService, private val billingService: BillingService) : BaseViewModel() {

    private val liveExport = MediatorLiveData<Export>()
    private val liveValidity = MediatorLiveData<Boolean>()
    val validity: LiveData<Boolean> = liveValidity


    private val liveContactList = MediatorLiveData<List<Contact>>()
    val contactList:LiveData<List<Contact>> = liveContactList


    init {
        liveExport.value = Export()
        liveValidity.addSource(liveExport) {
            liveValidity.value = checkValidity()
        }
    }


    fun updateContactList(){
        liveContactList.addSource(contactRepository.getContactList()){ result ->
            when(result.status){
                DataStatus.SUCCESS -> liveContactList.value = result.value
            }
        }
    }


    private fun checkValidity(): Boolean {
        var valid = true
        valid = valid && !liveExport.value?.receiptList.isNullOrEmpty()
        valid = valid && !liveExport.value?.fieldList.isNullOrEmpty()
        valid = valid && liveExport.value?.format != null
        valid = valid && !liveExport.value?.recipientList.isNullOrEmpty()
        return valid
    }

    fun export() {
        if (checkValidity()) {
            liveExport.value?.fieldList?.let {
                preferenceService.lastExportFields = mappingService.getFieldNames(it.toList())
            }
            liveExport.value?.format?.let {
                preferenceService.lastExportFormat = it
            }
            liveExport.value?.recipientList?.let {
                preferenceService.lastExportRecipients = it
            }
        } else {
            // TODO
        }
    }

    fun setExportReceiptList(list: List<Receipt>?) {
        list?.let {
            liveExport.value?.receiptList?.addAll(it.toList())
        }
    }

    fun setExportFieldList(list: List<String>?) {
        list?.let {
            liveExport.value?.fieldList?.clear()
            liveExport.value?.fieldList?.addAll(mappingService.getFields(it.toList()))
        }
    }

    fun setExportFormat(format: ExportFormat?) {
        format?.let {
            liveExport.value?.format = format
        }
    }

    fun setExportRecipientList(list: List<Contact>?) {
        list?.let {
            liveExport?.value?.recipientList?.addAll(it)
        }
    }



    fun getInitialFields() = mappingService.getClassMappingAliases(Receipt::class.java)
    fun getInitialSelectedFields() = preferenceService.lastExportFields
    fun getInitialPreviousContacts():List<Contact> = preferenceService.lastExportRecipients



}
