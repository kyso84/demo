package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class VolumeUnit(@StringRes val alias: Int) : Parcelable {

    LITER(R.string.liters),
    GALLONS_GB(R.string.gallons_gb),
    GALLONS_US(R.string.gallons_us),
    KW(R.string.kw);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VolumeUnit> {
        override fun createFromParcel(parcel: Parcel): VolumeUnit {
            return VolumeUnit.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<VolumeUnit?> {
            return arrayOfNulls(size)
        }
    }

    fun getGasTypeList(): List<GasType> = GasType.values().toList().filter { it.volumeUnitList.toList().contains(this) }
}