package com.kyso84.android.demo.datasource.remote

import com.kyso84.android.demo.model.car.CarMakesContainer
import com.kyso84.android.demo.model.car.CarModelsContainer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CarApi {

    @GET("?cmd=getMakes")
    fun getMakes(@Query("year") year: String): Call<CarMakesContainer>
    @GET("?cmd=getModels")
    fun getModels(@Query("make") make: String, @Query("year") year: String): Call<CarModelsContainer>
}