package com.kyso84.android.demo.util

object NumberUtil {
    @JvmStatic
    fun zeroSafe(n: Number) = if (n.toDouble() == 0.0) "" else n
}