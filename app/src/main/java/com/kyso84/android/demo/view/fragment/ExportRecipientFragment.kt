package com.kyso84.android.demo.view.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentExportRecipientBinding
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.view.adapter.ContactAdapter
import com.kyso84.android.demo.view.adapter.ExportRecipientAdapter
import com.kyso84.android.demo.viewmodel.ExportViewModel
import androidx.lifecycle.Observer
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.service.LocationService
import java.util.*
import android.widget.Toast
import android.os.Build
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import com.quicklib.android.core.helper.PermissionHelper


class ExportRecipientFragment : BaseFragment<FragmentExportRecipientBinding, ExportViewModel>(), PagerFragment {
    override fun getTitle() = com.kyso84.android.demo.R.string.recipients


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentExportRecipientBinding>(inflater, com.kyso84.android.demo.R.layout.fragment_export_recipient, container, false).root
    }


    override fun onViewReady(viewModel: ExportViewModel, savedInstanceState: Bundle?) {
        val adapter = ExportRecipientAdapter(layoutInflater)
        binding.fragmentExportRecipientList.adapter = adapter
        binding.fragmentExportRecipientList.layoutManager = LinearLayoutManager(context)
        adapter.setOnSelectionChangedListener(object : ExportRecipientAdapter.OnSelectionChangedListener {
            override fun onSelectionChanged(contactList: List<Contact>) {
                viewModel.setExportRecipientList(contactList)
            }
        })


        binding.fragmentExportRecipientSearch.setOnFocusChangeListener { v, hasFocus ->
            context?.let{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(it, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), Const.REQUEST_CONTACT_ACCESS)
                } else {
                }
            }
        }

        viewModel.contactList.observe(this, Observer {
            val contactList = it
            val suggestionAdapter = ContactAdapter(layoutInflater, object : Filter() {

                override fun performFiltering(constraint: CharSequence?): FilterResults {
                    val results = FilterResults()
                    results.values = contactList.filter { it.email.toLowerCase(Locale.getDefault()) == constraint.toString().toLowerCase(Locale.getDefault()) }
                    return results
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    val list: List<Contact> = results?.values as List<Contact>
                    (binding.fragmentExportRecipientSearch.adapter as ContactAdapter).addAll(list)
                }
            })
            binding.fragmentExportRecipientSearch.setAdapter(suggestionAdapter)
            binding.fragmentExportRecipientSearch.threshold = 2
        })
    }

    override fun getViewModelInstance(): ExportViewModel = ViewModelProviders.of(activity!!, this.viewModeFactory).get(ExportViewModel::class.java)


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.REQUEST_CONTACT_ACCESS -> if (resultCode == Activity.RESULT_OK) {
                data?.let {

                }
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == Const.REQUEST_CONTACT_ACCESS) {

        }
    }




}

