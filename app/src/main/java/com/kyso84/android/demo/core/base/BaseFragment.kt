package com.kyso84.android.demo.core.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.kyso84.android.demo.BR
import com.kyso84.android.demo.injection.*
import com.quicklib.android.mvvm.view.QuickFragment
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseFragment<VDB : ViewDataBinding, VM : ViewModel> : QuickFragment<VDB, VM>() {

    @Inject
    protected lateinit var appComponent: AppComponent
    protected lateinit var activityComponent: ActivityComponent
    protected lateinit var fragmentComponent: FragmentComponent

    @Inject
    lateinit var viewModeFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as HasSupportFragmentInjector).supportFragmentInjector().inject(this)
        activityComponent = (activity as BaseActivity<*, *>).activityComponent
        fragmentComponent = DaggerFragmentComponent.builder().activityComponent(activityComponent).fragment(this).build()
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        binding.setVariable(BR.viewModel, viewModel)
        onViewReady(viewModel, savedInstanceState)
    }

    abstract fun onViewReady(viewModel: VM, savedInstanceState: Bundle?)
}
