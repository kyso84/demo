package com.kyso84.android.demo.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseActivity
import com.kyso84.android.demo.databinding.ActivityCarCreatorBinding
import com.kyso84.android.demo.viewmodel.VoidViewModel
import javax.inject.Inject

class CarCreatorActivity : BaseActivity<ActivityCarCreatorBinding, VoidViewModel>() {

    companion object {
        const val TAG = "OnBoardingActivity"
    }

    @Inject
    lateinit var router: Router

    override fun getViewModelInstance(): VoidViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(VoidViewModel::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityCarCreatorBinding>(this, R.layout.activity_car_creator)
    }

    override fun onViewReady(viewModel: VoidViewModel, savedInstanceState: Bundle?) {
    }
}
