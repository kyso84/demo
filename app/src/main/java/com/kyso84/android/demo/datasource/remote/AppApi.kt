package com.kyso84.android.demo.datasource.remote

import com.kyso84.android.demo.model.Receipt
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface AppApi {
    @GET("receipts")
    fun getReceiptList(): Deferred<AppResponse<List<Receipt>>>
    @POST("receipts")
    fun addReceipt(@Body receipt: Receipt): Deferred<AppResponse<Receipt>>
}