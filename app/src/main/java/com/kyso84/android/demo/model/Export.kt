package com.kyso84.android.demo.model

import androidx.lifecycle.MutableLiveData
import java.lang.reflect.Field

class Export(
    var receiptList: MutableList<Receipt> = mutableListOf(),
    var fieldList: MutableList<Field> = mutableListOf(),
    var format: ExportFormat = ExportFormat.CSV,
    var recipientList: MutableList<Contact> = mutableListOf()
)