package com.kyso84.android.demo.injection.injector

import com.kyso84.android.demo.view.fragment.CarFormFragment
import com.kyso84.android.demo.view.fragment.CarPickerFragment
import com.kyso84.android.demo.view.fragment.ExportFieldFragment
import com.kyso84.android.demo.view.fragment.ExportFormatFragment
import com.kyso84.android.demo.view.fragment.ExportRecipientFragment
import com.kyso84.android.demo.view.fragment.GasStationViewerFragment
import com.kyso84.android.demo.view.fragment.ReceiptFormFragment
import com.kyso84.android.demo.view.fragment.ReceiptListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentInjectorModule {
    @ContributesAndroidInjector
    abstract fun contributeReceiptFormFragment(): ReceiptFormFragment
    @ContributesAndroidInjector
    abstract fun contributeReceiptListFragment(): ReceiptListFragment
    @ContributesAndroidInjector
    abstract fun contributeCarFormFragment(): CarFormFragment
    @ContributesAndroidInjector
    abstract fun contributeCarPickerFragment(): CarPickerFragment
    @ContributesAndroidInjector
    abstract fun contributeExportFieldFragment(): ExportFieldFragment
    @ContributesAndroidInjector
    abstract fun contributeExportFormatFragment(): ExportFormatFragment
    @ContributesAndroidInjector
    abstract fun contributeExportRecipientFragment(): ExportRecipientFragment
    @ContributesAndroidInjector
    abstract fun contributeGasStationViewerFragment(): GasStationViewerFragment
}
