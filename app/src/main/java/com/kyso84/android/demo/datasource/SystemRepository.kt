package com.kyso84.android.demo.datasource

import androidx.lifecycle.LiveData
import com.quicklib.android.network.DataWrapper

interface SystemRepository {

    fun fetch(): LiveData<DataWrapper<Boolean>>

    fun getPublishedVersionCode():Int
    fun isOnBoardingEnabled():Boolean
    fun getStartCounter(): Int
    fun getSplashScreenDelay(): Long
}