package com.kyso84.android.demo.injection.scope

import androidx.lifecycle.ViewModel
import dagger.MapKey
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import kotlin.reflect.KClass

@Documented
@Retention(RetentionPolicy.RUNTIME)
@MapKey
internal annotation class ViewModelScope(val value: KClass<out ViewModel>)
