package com.kyso84.android.demo.datasource

import android.location.Location
import androidx.lifecycle.LiveData
import com.kyso84.android.demo.model.GasStation
import com.quicklib.android.network.DataWrapper

interface GeoRepository  {
    fun getClosestGasStation(location: Location):LiveData<DataWrapper<GasStation?>>
    fun getClosestGasStationList(location: Location) :LiveData<DataWrapper<List<GasStation>>>
}