package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.datasource.local.AppDao
import com.kyso84.android.demo.datasource.remote.AppApi
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.LocalDataAwareFirstStrategy
import com.quicklib.android.network.strategy.LocalDataOnlyStrategy
import com.quicklib.android.network.strategy.WriteLocalDataStrategy
import com.quicklib.android.network.strategy.WriteRemoteDataStrategy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class AppManagedRepository(private val authRepository: AuthRepository,  private val api: AppApi, private val dao: AppDao) : AppRepository {


    override fun addReceipt(receipt: Receipt) = object : WriteRemoteDataStrategy<Receipt>(receipt) {
        override fun dataSaved(data: Receipt) {
            dao.insertReceipt(data)
        }

        override suspend fun writeDataRemote(data: Receipt): Deferred<Receipt> {
            val res = api.addReceipt( data).await()
            return CompletableDeferred(res.getResult())
        }
    }.asLiveData()

    override fun getReceiptList(): LiveData<DataWrapper<List<Receipt>>> = object : LocalDataAwareFirstStrategy<List<Receipt>>() {
        override suspend fun readData(): Deferred<LiveData<List<Receipt>>> = CompletableDeferred(dao.watchReceiptList())
        override suspend fun fetchData(): Deferred<List<Receipt>> {
            val res = api.getReceiptList().await()
            return CompletableDeferred(res.getResult())
        }

        override suspend fun writeData(data: List<Receipt>) {
            dao.insertReceipt(data)
        }
    }.asLiveData()

    override fun addGasStation(gasStation: GasStation) = object : WriteLocalDataStrategy<GasStation>(gasStation) {
        override suspend fun writeData(data: GasStation): Deferred<GasStation> {
            dao.insertGasStation(gasStation)
            return CompletableDeferred(gasStation)
        }
    }.asLiveData()

    override fun getGasStation(gasStationId: String): LiveData<DataWrapper<GasStation>> = object : LocalDataOnlyStrategy<GasStation>() {
        override suspend fun readData(): Deferred<GasStation> = CompletableDeferred(dao.getGasStation(gasStationId))
    }.asLiveData()

    override fun getGasStationList(): LiveData<DataWrapper<List<GasStation>>> = object : LocalDataOnlyStrategy<List<GasStation>>() {
        override suspend fun readData(): Deferred<List<GasStation>> = CompletableDeferred(dao.getGasStationList())
    }.asLiveData()

    override fun addCar(car: Car) = object : WriteLocalDataStrategy<Car>(car) {
        override suspend fun writeData(data: Car): Deferred<Car> {
            dao.insertCar(car)
            return CompletableDeferred(car)
        }
    }.asLiveData()

    override fun getCar(carId: String): LiveData<DataWrapper<Car>> = object : LocalDataOnlyStrategy<Car>() {
        override suspend fun readData(): Deferred<Car> = CompletableDeferred(dao.getCar(carId))
    }.asLiveData()

    override fun getLatestCar(): LiveData<DataWrapper<Car?>> = object : LocalDataOnlyStrategy<Car?>() {
        override suspend fun readData(): Deferred<Car?> = CompletableDeferred(dao.getCarList().firstOrNull())
    }.asLiveData()

    override fun getCarList(): LiveData<DataWrapper<List<Car>>> = object : LocalDataOnlyStrategy<List<Car>>() {
        override suspend fun readData(): Deferred<List<Car>> = CompletableDeferred(dao.getCarList())
    }.asLiveData()
}