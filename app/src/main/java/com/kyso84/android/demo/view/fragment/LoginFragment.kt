package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.view.* // ktlint-disable no-wildcard-imports
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentLoginBinding
import javax.inject.Inject
import com.kyso84.android.demo.viewmodel.LoginViewModel
import com.kyso84.android.demo.viewmodel.OnBoardingViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(), OnBoardingViewModel.OnBoardingView<LoginFragment> {

    companion object {
        const val TAG = "CarFormFragment"
    }

    @Inject
    lateinit var router: Router

    override fun getViewModelInstance(): LoginViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(LoginViewModel::class.java)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentLoginBinding>(inflater, com.kyso84.android.demo.R.layout.fragment_login, container, false).root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewReady(viewModel: LoginViewModel, savedInstanceState: Bundle?) {
    }
}
