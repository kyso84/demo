package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.CarRepository
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasTypeGroup
import com.kyso84.android.demo.model.car.CarMake
import com.kyso84.android.demo.model.car.CarModel
import com.quicklib.android.network.DataStatus
import java.util.Calendar
import javax.inject.Inject

class CarFormViewModel @Inject constructor(private val repo: AppRepository, private val carRepository: CarRepository) : BaseViewModel() {

    companion object {
        const val TAG = "CarFormViewModel"
    }

    private val liveYearList = MediatorLiveData<List<String>>()
    val yearList: LiveData<List<String>> = liveYearList

    private val liveMakeList = MediatorLiveData<List<CarMake>>()
    val makeList: LiveData<List<CarMake>> = liveMakeList

    private val liveModelList = MediatorLiveData<List<CarModel>>()
    val modelList: LiveData<List<CarModel>> = liveModelList

    val year = MutableLiveData<String>()
    val make = MutableLiveData<CarMake>()
    val model = MutableLiveData<CarModel>()
    val alias = MutableLiveData<String>()
    val gasTypeGroup = MutableLiveData<GasTypeGroup>()

    var carId: String =  ""
    val car = MediatorLiveData<Car>()

    init {
        liveYearList.value = createYearList()
        val sourceMake = Transformations.switchMap(year) {
            carRepository.getMakeList(it)
        }
        liveMakeList.addSource(sourceMake) { result ->
            when (result.status) {
                DataStatus.SUCCESS -> {
                    liveMakeList.value = result.value
                }
                DataStatus.ERROR -> {
                    liveMakeList.value = null
                }
            }
        }

        val sourceModel = Transformations.switchMap(make) {
            carRepository.getModelList(year.value ?: "", it)
        }
        liveModelList.addSource(sourceModel) { result ->
            when (result.status) {
                DataStatus.SUCCESS -> {
                    liveModelList.value = result.value
                }
                DataStatus.ERROR -> {
                    liveModelList.value = null
                }
            }
        }

        car.addSource(year) {
            car.value = validate()
        }
        car.addSource(make) {
            car.value = validate()
        }
        car.addSource(model) {
            car.value = validate()
        }
        car.addSource(gasTypeGroup) {
            car.value = validate()
        }
    }

    fun setCar(car: Car?) {
        car?.let {
            carId = it.id
            year.value = it.year
            make.value = CarMake(id = it.makeId ?: "", name = it.modelName, country = it.country)
            model.value = CarModel(name = it.modelName, makeId = it.makeId ?: "", year = it.year)
        }
    }

    private fun createYearList(): List<String> {
        val list = mutableListOf<String>()
        for (x in 1941 until Calendar.getInstance().get(Calendar.YEAR) + 1) {
            list.add(x.toString())
        }
        return list.asReversed()
    }

    private fun validate(): Car? {
        year.value?.let { year ->
            make.value?.let { carMake ->
                model.value?.let { carModel ->
                    gasTypeGroup.value?.let { gasTypeGroup ->
                        return Car(
                                id = carId,
                                makeId = carMake.id,
                                makeName = carMake.name,
                                modelName = carModel.name,
                                year = year,
                                country = carMake.country,
                                gasTypeGroup = gasTypeGroup,
                                alias = alias.value ?: "",
                                imageUrl = ""
                        )
                    }
                }
            }
        }
        return null
    }

    fun save(): Boolean {
        car.value?.let {
            repo.addCar(it)
            return true
        }
        return false
    }
}