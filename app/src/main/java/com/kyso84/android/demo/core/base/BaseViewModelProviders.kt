package com.kyso84.android.demo.core.base

import android.app.Application
import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.Factory
import androidx.lifecycle.ViewModelProviders

open class BaseViewModelProviders : ViewModelProviders() {
    /**
     * Creates a [ViewModelProvider], which retains ViewModels while a scope of given
     * `RecyclerView.ViewHolder` is alive. More detailed explanation is in [ViewModel].
     *
     *
     * It uses the given [Factory] to instantiate new ViewModels.
     *
     * @param fragment a fragment, in whose scope ViewModels should be retained
     * @param factory a `Factory` to instantiate new ViewModels
     * @return a ViewModelProvider instance
     */
    @MainThread
    companion object {
        fun of(viewHolder: BaseViewHolder<*, *, *>, factory: Factory?): ViewModelProvider {
            var factory = factory
            val application = viewHolder.itemView.context.applicationContext as Application
            if (factory == null) {
                factory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            }
            return ViewModelProvider(viewHolder.viewModelStore, factory)
        }
    }
}