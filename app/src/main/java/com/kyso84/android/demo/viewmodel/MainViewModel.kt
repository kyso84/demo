package com.kyso84.android.demo.viewmodel

import com.kyso84.android.demo.core.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : BaseViewModel()
