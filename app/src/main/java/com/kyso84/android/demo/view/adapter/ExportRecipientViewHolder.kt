package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.databinding.PartExportFormatRowBinding
import com.kyso84.android.demo.databinding.PartExportRecipientRowBinding
import com.kyso84.android.demo.model.Contact

class ExportRecipientViewHolder(val binding : PartExportRecipientRowBinding) :RecyclerView.ViewHolder( binding.root) {
    constructor(layoutInflater: LayoutInflater, container: ViewGroup?):this(PartExportRecipientRowBinding.inflate(layoutInflater, container, false))


    fun bind(contact: Contact) {
        binding.partExportRecipientRowTitle.text = contact.email
    }
}