package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class ExportFormat(@StringRes val titleId: Int, @DrawableRes val iconId: Int) : Parcelable {

    CSV(R.string.export_format_csv, R.drawable.ic_select_all);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExportFormat> {
        override fun createFromParcel(parcel: Parcel): ExportFormat {
            return ExportFormat.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<ExportFormat?> {
            return arrayOfNulls(size)
        }
    }
}