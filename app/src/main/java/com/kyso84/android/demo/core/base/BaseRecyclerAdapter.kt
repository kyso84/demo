package com.kyso84.android.demo.core.base

import android.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.quicklib.android.recyclerview.adapter.RecyclerListAdapter

abstract class BaseRecyclerAdapter<T, VH:RecyclerView.ViewHolder>() : RecyclerListAdapter<T, VH>(mutableListOf())