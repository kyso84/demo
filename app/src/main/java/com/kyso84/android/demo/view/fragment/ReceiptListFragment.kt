package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.HORIZONTAL
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentReceiptListBinding
import com.kyso84.android.demo.util.MultiSelectionManager
import com.kyso84.android.demo.view.adapter.ReceiptRecyclerAdapter
import com.kyso84.android.demo.viewmodel.ReceiptListViewModel
import javax.inject.Inject

class ReceiptListFragment : BaseFragment<FragmentReceiptListBinding, ReceiptListViewModel>() {

    companion object {
        const val TAG = "ReceiptListFragment"
    }

    @Inject
    lateinit var router: Router
    lateinit var adapter: ReceiptRecyclerAdapter

    private var actionMode: ActionMode? = null
    private var multiSelectionManager: MultiSelectionManager? = null

    override fun getViewModelInstance(): ReceiptListViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(ReceiptListViewModel::class.java)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentReceiptListBinding>(inflater, R.layout.fragment_receipt_list, container, false).root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewReady(viewModel: ReceiptListViewModel, savedInstanceState: Bundle?) {
        multiSelectionManager = object : MultiSelectionManager() {
            override fun onMultiSelectionEnabled() {
                actionMode = activity?.startActionMode(actionCallback)
                actionMode?.setTitle(R.string.receipt_to_export)
            }

            override fun onMultiSelectionDisabled() {
                actionMode?.finish()
                actionMode = null
            }
        }
        adapter = ReceiptRecyclerAdapter(fragment = this, multiSelectionManager = multiSelectionManager)

        binding.router = router
        binding.fragmentReceiptList.adapter = adapter
        binding.fragmentReceiptList.addItemDecoration(DividerItemDecoration(binding.fragmentReceiptList.context, HORIZONTAL))
        binding.fragmentReceiptListRefresh.setOnRefreshListener {
            viewModel.refresh()
            binding.fragmentReceiptListRefresh.isRefreshing = false
        }
        binding.fragmentReceiptListButton.setOnClickListener {
            viewModel.getLatestCar().observe(this, Observer {
                router.showReceiptCreator(binding.root, it)
            })
        }
        viewModel.list.observe(this, Observer {
            adapter.clear()
            adapter.addAll(it)
        })
        viewModel.refresh()
    }

    override fun onPause() {
        super.onPause()
        actionMode?.finish()
        actionMode = null
    }

    private val actionCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.receipt_context_menu, menu)

            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean = false

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.receipt_context_menu_export -> {
                    activity?.let {
                        router.openExport(it, {}, *adapter.getSelectedItems().toTypedArray())
                    }
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            multiSelectionManager?.clearSelections()
            binding.fragmentReceiptList.adapter = adapter
        }
    }
}