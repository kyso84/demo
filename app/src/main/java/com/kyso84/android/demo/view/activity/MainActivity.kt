package com.kyso84.android.demo.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.base.BaseActivity
import com.kyso84.android.demo.databinding.ActivityMainBinding
import com.kyso84.android.demo.viewmodel.MainViewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    companion object {
        const val TAG = "MainActivity"
    }

    override fun getViewModelInstance(): MainViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(MainViewModel::class.java)
    override fun onSupportNavigateUp() = findNavController(R.id.activity_main_nav).navigateUp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onViewReady(viewModel: MainViewModel, savedInstanceState: Bundle?) {
        setSupportActionBar(binding.activityMainToolbar)
    }
}
