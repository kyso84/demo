package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class Pack(@StringRes val title: Int, @StringRes val description: Int) : Parcelable {
    CAR_PACK(R.string.car_pack_title, R.string.car_pack_description),
    EXPORT_PACK(R.string.export_pack_title, R.string.export_pack_description);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pack> {
        override fun createFromParcel(parcel: Parcel): Pack {
            return Pack.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<Pack?> {
            return arrayOfNulls(size)
        }
    }
}