package com.kyso84.android.demo.model

import com.google.firebase.auth.FirebaseUser

class User(
    val id: String,
    val displayName: String?,
    val email:String?,
    val anonymous:Boolean
){
    constructor(firebaseUser: FirebaseUser) : this(firebaseUser.uid, firebaseUser.displayName, firebaseUser.email, firebaseUser.isAnonymous){

    }
}