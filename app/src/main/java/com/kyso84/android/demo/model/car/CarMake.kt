package com.kyso84.android.demo.model.car

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CarMake(
    @SerializedName("make_id")
    val id: String,
    @SerializedName("make_display")
    val name: String,
    @SerializedName("make_country")
    val country: String
) : Parcelable, Comparable<CarMake> {
    override fun compareTo(other: CarMake): Int = id.compareTo(other.id)

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(country)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return name
    }

    companion object CREATOR : Parcelable.Creator<CarMake> {
        override fun createFromParcel(parcel: Parcel): CarMake {
            return CarMake(parcel)
        }

        override fun newArray(size: Int): Array<CarMake?> {
            return arrayOfNulls(size)
        }
    }
}