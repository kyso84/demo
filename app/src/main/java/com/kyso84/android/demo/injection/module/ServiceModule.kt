package com.kyso84.android.demo.injection.module

import com.kyso84.android.demo.core.service.BillingService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServiceModule {
    @Provides
    @Singleton
    fun provideBillingService() = BillingService()
}