package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.util.MultiSelectionManager
import javax.inject.Inject

class ReceiptViewModel @Inject constructor(private val repository: AppRepository, private val preferenceService: PreferenceService) : BaseViewModel() {

    private val liveReceipt = MutableLiveData<Receipt>()
    val receipt: LiveData<Receipt>
        get() = liveReceipt

    val selected = MutableLiveData<MultiSelectionManager.Status>()

    fun setReceipt(receipt: Receipt) {
        liveReceipt.value = receipt
    }
}