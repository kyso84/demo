package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import com.kyso84.android.demo.R

enum class GasTypeGroup(@StringRes val title: Int, vararg val gasTypeList: GasType) : Parcelable {

    GAZOLINE(R.string.gastype_group_gazoline, GasType.REGULAR, GasType.MEDIUM, GasType.PREMIUM),
    DIESEL(R.string.gastype_group_diesel, GasType.DIESEL),
    ELECTRIC(R.string.gastype_group_electric, GasType.ELECTRIC);

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GasTypeGroup> {
        override fun createFromParcel(parcel: Parcel): GasTypeGroup {
            return GasTypeGroup.values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<GasTypeGroup?> {
            return arrayOfNulls(size)
        }
    }
}