package com.kyso84.android.demo.injection.injector

import com.kyso84.android.demo.view.activity.CarCreatorActivity
import com.kyso84.android.demo.view.activity.ExportActivity
import com.kyso84.android.demo.view.activity.MainActivity
import com.kyso84.android.demo.view.activity.OnBoardingActivity
import com.kyso84.android.demo.view.activity.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectorModule {
    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeSplashActivity(): SplashActivity
    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeMainActivity(): MainActivity
    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeExportActivity(): ExportActivity
    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeOnBoardingActivity(): OnBoardingActivity
    @ContributesAndroidInjector(modules = [FragmentInjectorModule::class])
    abstract fun contributeCarCreatorActivity(): CarCreatorActivity
}
