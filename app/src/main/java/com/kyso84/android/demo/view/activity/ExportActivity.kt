package com.kyso84.android.demo.view.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseActivity
import com.kyso84.android.demo.databinding.ActivityExportBinding
import com.kyso84.android.demo.view.adapter.ExportPagerAdapter
import com.kyso84.android.demo.viewmodel.ExportViewModel
import javax.inject.Inject

class ExportActivity : BaseActivity<ActivityExportBinding, ExportViewModel>() {

    companion object {
        const val TAG = "ExportActivity"
    }

    @Inject
    lateinit var router: Router

    private var menuGo: MenuItem? = null

    override fun getViewModelInstance(): ExportViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(ExportViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityExportBinding>(this, R.layout.activity_export)
    }

    override fun onViewReady(viewModel: ExportViewModel, savedInstanceState: Bundle?) {
        setSupportActionBar(binding.activityExportToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setTitle(R.string.export)

        val adapter = ExportPagerAdapter(applicationContext ,supportFragmentManager)
        binding.activityExportPager.adapter = adapter

        viewModel.validity.observe(this, Observer {
            menuGo?.isEnabled = it
        })





        viewModel.setExportReceiptList(intent?.extras?.getParcelableArrayList(Const.KEY_RECEIPT_LIST))
        viewModel.setExportFieldList(intent?.extras?.getStringArrayList(Const.KEY_FIELD_LIST))
        viewModel.setExportFormat(intent?.extras?.getParcelable(Const.KEY_FORMAT))
        viewModel.setExportRecipientList(intent?.extras?.getParcelableArrayList(Const.KEY_RECIPIENT_LIST))
        adapter.setArguments(intent?.extras)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.export_menu, menu)
        menuGo = menu.findItem(R.id.export_menu_go)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.export_menu_go -> {

                true
            }
            else -> false
        }
    }

}
