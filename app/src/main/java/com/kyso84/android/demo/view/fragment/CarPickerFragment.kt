package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.Router
import com.kyso84.android.demo.core.base.BaseFragment
import com.kyso84.android.demo.databinding.FragmentReceiptListBinding
import com.kyso84.android.demo.viewmodel.CarPickerViewModel
import com.kyso84.android.demo.databinding.FragmentCarPickerBinding
import javax.inject.Inject

class CarPickerFragment : BaseFragment<FragmentCarPickerBinding, CarPickerViewModel>() {

    companion object {
        const val TAG = "ReceiptListFragment"
    }

    @Inject
    lateinit var router: Router

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentReceiptListBinding>(inflater, R.layout.fragment_car_picker, container, false).root
    }

    override fun getViewModelInstance(): CarPickerViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(CarPickerViewModel::class.java)

    override fun onViewReady(viewModel: CarPickerViewModel, savedInstanceState: Bundle?) {
// TODO
    }
}