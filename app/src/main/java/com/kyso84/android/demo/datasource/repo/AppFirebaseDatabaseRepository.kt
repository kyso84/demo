package com.kyso84.android.demo.datasource.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.AuthRepository
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataStatus
import com.quicklib.android.network.DataWrapper
import com.quicklib.android.network.strategy.FirebaseDatabaseReadListStrategy
import com.quicklib.android.network.strategy.FirebaseDatabaseReadObjectStrategy
import com.quicklib.android.network.strategy.FirebaseDatabaseWriteStrategy


class AppFirebaseDatabaseRepository(private val authRepository: AuthRepository, private val firebaseDatabaseRoot: FirebaseDatabase) : AppRepository {
    companion object {
        const val ENDPOINT_RECEIPT = "receipts"
        const val ENDPOINT_GAS_STATION = "gas_stations"
        const val ENDPOINT_CARS = "cars"
    }

    private fun getDatabase():DatabaseReference = authRepository.getUser()?.id?.let{getDatabasePrivate(it)}?:getDatabasePublic()
    private fun getDatabasePrivate(userId:String):DatabaseReference =firebaseDatabaseRoot.getReference("v1/users/$userId")
    private fun getDatabasePublic():DatabaseReference = firebaseDatabaseRoot.getReference("v1/public")

    
    override fun addReceipt(receipt: Receipt): LiveData<DataWrapper<Receipt>> = object : FirebaseDatabaseWriteStrategy<Receipt>(getDatabase().child(ENDPOINT_RECEIPT), receipt,
            dataId = { receipt.id },
            newDataId = { newId -> receipt.addId(newId) }) {

    }.asLiveData()

    override fun getReceiptList(): LiveData<DataWrapper<List<Receipt>>> = object : FirebaseDatabaseReadListStrategy<Receipt>(getDatabase().child(ENDPOINT_RECEIPT)) {
        override fun deserialize(snapshot: DataSnapshot): List<Receipt> {
            val result = mutableListOf<Receipt>()
            snapshot.children.forEach { it.getValue(Receipt::class.java)?.let { result.add(it) } }
            return result
        }
    }.asLiveData()

    override fun addGasStation(gasStation: GasStation): LiveData<DataWrapper<GasStation>> = object : FirebaseDatabaseWriteStrategy<GasStation>(getDatabasePublic().child(ENDPOINT_GAS_STATION), gasStation,
            dataId = { gasStation.id },
            newDataId = { newId -> gasStation.addId(newId) }) {

    }.asLiveData()


    override fun getGasStation(gasStationId: String): LiveData<DataWrapper<GasStation>> = object : FirebaseDatabaseReadObjectStrategy<GasStation>(getDatabasePublic().child(ENDPOINT_GAS_STATION).child(gasStationId)) {
        override fun deserialize(snapshot: DataSnapshot): GasStation? = snapshot.getValue(GasStation::class.java)
    }.asLiveData()

    override fun getGasStationList(): LiveData<DataWrapper<List<GasStation>>> = object : FirebaseDatabaseReadListStrategy<GasStation>(getDatabasePublic().child(ENDPOINT_GAS_STATION)) {
        override fun deserialize(snapshot: DataSnapshot): List<GasStation> {
            val result = mutableListOf<GasStation>()
            snapshot.children.forEach { it.getValue(GasStation::class.java)?.let { result.add(it) } }
            return result
        }
    }.asLiveData()

    override fun addCar(car: Car): LiveData<DataWrapper<Car>> = object : FirebaseDatabaseWriteStrategy<Car>(getDatabase().child(ENDPOINT_CARS), car,
            dataId = { car.id },
            newDataId = { newId -> car.addId(newId) }) {

    }.asLiveData()

    override fun getCar(carId: String): LiveData<DataWrapper<Car>> = object : FirebaseDatabaseReadObjectStrategy<Car>(getDatabase().child(ENDPOINT_CARS).child(carId)) {
        override fun deserialize(snapshot: DataSnapshot): Car? = snapshot.getValue(Car::class.java)
    }.asLiveData()

    override fun getLatestCar(): LiveData<DataWrapper<Car?>> {
        val result = MutableLiveData<DataWrapper<Car?>>()
        getDatabase().child(ENDPOINT_CARS).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (!snapshot.exists() || snapshot.hasChildren()) {
                    val car = snapshot.children.first()?.getValue(Car::class.java)
                    result.postValue(DataWrapper(value = car, status = DataStatus.SUCCESS, localData = true, strategy = null))
                } else {
                    result.postValue(DataWrapper(error = IllegalStateException("Type mismatch: A list is expected but a object was found. If truly want a list, please use FirebaseDataStrategy instead"), status = DataStatus.ERROR, localData = true, strategy = null))
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                result.postValue(DataWrapper(error = databaseError.toException(), status = DataStatus.ERROR, localData = true, strategy = null))
            }
        })
        return result
    }

    override fun getCarList(): LiveData<DataWrapper<List<Car>>> = object : FirebaseDatabaseReadListStrategy<Car>(getDatabase().child(ENDPOINT_CARS)) {
        override fun deserialize(snapshot: DataSnapshot): List<Car> {
            val result = mutableListOf<Car>()
            snapshot.children.forEach { it.getValue(Car::class.java)?.let { result.add(it) } }
            return result
        }
    }.asLiveData()

}