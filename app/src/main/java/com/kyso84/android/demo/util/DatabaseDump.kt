package com.kyso84.android.demo.util

import android.app.Activity
import android.os.Environment
import android.util.Log
import androidx.core.app.ShareCompat
import com.google.gson.Gson
import com.kyso84.android.demo.datasource.remote.CarApi
import com.kyso84.android.demo.model.car.CarMake
import com.kyso84.android.demo.model.car.CarModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import java.util.Calendar

class DatabaseDump(val activity: Activity, val gson: Gson, val carApi: CarApi) {

    fun dumping() {
        GlobalScope.launch {

            val yearList = createYearList()
            val makeList = mutableListOf<CarMake>()
            val modelList = mutableListOf<CarModel>()

            yearList.forEach { year ->
                val makeResult: List<CarMake> = carApi.getMakes(year).execute().body()?.Makes
                        ?: listOf()
                makeList.addAll(makeResult)
                Log.d("XXX", gson.toJson("$year - ${makeList.size} makes"))
                delay(getSleepDuration())
            }
            val finalMakeList = makeList.distinctBy { it.id }
            Log.d("XXX", gson.toJson("FINAL MAKE - ${gson.toJson(finalMakeList)}"))
            yearList.forEach { year ->
                finalMakeList.forEach { make ->
                    val modelResult: List<CarModel> = carApi.getModels(make.id, year).execute().body()?.Models
                            ?: listOf()
                    modelList.addAll(modelResult)
                    Log.d("XXX", gson.toJson("$year - ${make.name} - ${modelList.size} models"))
                    delay(getSleepDuration())
                }
            }
            val finalModelLists = modelList.distinctBy { "${it.makeId}_${it.name}" }
            Log.d("XXX", gson.toJson("FINAL MODELS - ${gson.toJson(finalModelLists)}"))

            writeToFile("make.json", gson.toJson(finalMakeList))
            writeToFile("model.json", gson.toJson(finalModelLists))
            email("kyso84@gmail.com", "Export", listOf("make.json", "model.json"))
        }
    }

    private fun getSleepDuration(): Long = (2000..5000).random().toLong()
    private fun createYearList(): List<String> {
        val list = mutableListOf<String>()
        for (x in 1970 until Calendar.getInstance().get(Calendar.YEAR) + 1) {
            list.add(x.toString())
        }
        return list.asReversed()
    }

    private fun writeToFile(file: String, data: String) {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val file = File(activity.filesDir, file)
        file.writeText(data)
    }

    private fun email(emailTo: String, subject: String, filePaths: List<String>) {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val buffer = StringBuffer()
        filePaths.forEach {
            val file = File(activity.filesDir, it)
            buffer.append(file.readText())
            buffer.append("\n\n\n\n")
        }
        ShareCompat.IntentBuilder.from(activity)
                .setType("message/rfc822")
                .addEmailTo(emailTo)
                .setSubject(subject)
                .setText(buffer.toString())
                .setChooserTitle("TEST")
                .startChooser()
    }
}