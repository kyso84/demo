package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.datasource.AppRepository
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.Receipt
import com.quicklib.android.network.DataStatus
import javax.inject.Inject

class ReceiptListViewModel @Inject constructor(private val repository: AppRepository, private val preferenceService: PreferenceService) : BaseViewModel() {

    private val liveList = MediatorLiveData<List<Receipt>>()
    val list: LiveData<List<Receipt>>
        get() = liveList

    init {
        refresh()
    }

    fun refresh() {
        liveList.removeSource(repository.getReceiptList())
        liveList.addSource(repository.getReceiptList()) {
            when (it.status) {
                DataStatus.SUCCESS -> {
                    liveList.value = it.value
                }
                else -> {
                    // Show error
                }
            }
        }
    }

    fun getLatestCar(): LiveData<Car> {
        val car = MediatorLiveData<Car>()
        car.addSource(repository.getLatestCar()) { result ->
            when (result.status) {
                DataStatus.SUCCESS -> {
                    car.value = result.value
                }
                DataStatus.ERROR -> {
                    car.value = null
                }
            }
        }
        return car
    }
}