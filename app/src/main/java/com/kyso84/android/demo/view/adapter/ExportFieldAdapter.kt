package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.core.SimpleItemTouchHelperCallback
import kotlinx.android.synthetic.main.part_export_field_row.view.*

class ExportFieldAdapter(private val layoutInflater: LayoutInflater, private val items: MutableList<String> = mutableListOf(), private val selectedItems: MutableList<String> = mutableListOf(), private val reorder: Boolean = true) : RecyclerView.Adapter<ExportFieldViewHolder>(), SimpleItemTouchHelperCallback.TouchAdapter {

    private var listener: OnSelectionChangedListener? = null
    private var touchHelper: ItemTouchHelper? = null

    interface OnSelectionChangedListener {
        fun onSelectionChanged(list: List<String>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExportFieldViewHolder = ExportFieldViewHolder(layoutInflater, parent)

    override fun getItemCount(): Int = items.size


    fun getSelectedItems(): List<String> = items.filter { selectedItems.contains(it) }.toList()

    override fun onBindViewHolder(holder: ExportFieldViewHolder, position: Int) {
        val field = items[position]

        holder.bind(field, selectedItems.contains(field))
        holder.binding.partExportFieldRowTitle.setOnClickListener {
            toggleSelection(field)
        }
        holder.itemView.part_export_field_row_drag_holder.setOnTouchListener { v, event ->
            touchHelper?.startDrag(holder)
            true
        }
    }

    private fun toggleSelection(item: String) {
        val position = items.indexOf(item)
        if (selectedItems.contains(item)) {
            selectedItems.remove(item)
        } else {
            selectedItems.add(item)
        }

        if (reorder) {
            items.sortBy { !selectedItems.contains(it) }
            notifyDataSetChanged()
        } else {
            notifyItemChanged(position)
        }
        listener?.onSelectionChanged(getSelectedItems())
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        val item = items.removeAt(fromPosition)
        val wasSelected = selectedItems.contains(item)
        items.add(toPosition, item)
        notifyItemMoved(fromPosition, toPosition)

        if (wasSelected) {
            listener?.onSelectionChanged(getSelectedItems())
        }
    }

    override fun onItemDismiss(position: Int) {
        val item = selectedItems.removeAt(position)
        val wasSelected = selectedItems.contains(item)
        selectedItems.remove(item)
        notifyItemRemoved(position)

        if (wasSelected) {
            listener?.onSelectionChanged(getSelectedItems())
        }
    }

    fun setItems(list: List<String>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }


    fun setSelectedItems(list: List<String>) {
        selectedItems.clear()
        selectedItems.addAll(list)
        if (reorder) {
            items.sortBy { !selectedItems.contains(it) }
        }
        notifyDataSetChanged()
    }

    fun selectAll() {
        selectedItems.clear()
        selectedItems.addAll(items)
        notifyDataSetChanged()
    }

    fun unselectAll() {
        selectedItems.clear()
        notifyDataSetChanged()
    }

    fun setOnSelectionChangedListener(listener: OnSelectionChangedListener) {
        this.listener = listener
    }

    fun setTouchHelper(touchHelper: ItemTouchHelper) {
        this.touchHelper = touchHelper
    }
}