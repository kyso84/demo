package com.kyso84.android.demo.viewmodel

import androidx.lifecycle.MutableLiveData
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.model.GasStation
import javax.inject.Inject
import javax.inject.Named

class GasStationViewModel @Inject constructor(@Named(Const.KEY_MAPS_API_KEY) val mapsApiKey: String) : BaseViewModel() {

    val gasStation = MutableLiveData<GasStation>()
    val zoom = 15

    companion object {

        const val TAG = "GasStationViewModel"
    }


    fun start(gasStation: GasStation) {
        this.gasStation.value = gasStation
    }

}
