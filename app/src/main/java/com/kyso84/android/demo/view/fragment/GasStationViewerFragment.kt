package com.kyso84.android.demo.view.fragment

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kyso84.android.demo.core.Const
import com.kyso84.android.demo.core.base.BaseDialogFragment
import com.kyso84.android.demo.databinding.FragmentGasStationViewerBinding
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.view.adapter.ImageCarouselAdapter
import com.kyso84.android.demo.viewmodel.GasStationViewModel

class GasStationViewerFragment : BaseDialogFragment<FragmentGasStationViewerBinding, GasStationViewModel>() {
    override fun getViewModelInstance(): GasStationViewModel = ViewModelProviders.of(this, this.viewModeFactory).get(GasStationViewModel::class.java)

    override fun onViewReady(viewModel: GasStationViewModel, savedInstanceState: Bundle?) {
        viewModel.gasStation.observe(this, Observer {
            binding.fragmentGasStationViewerPager.adapter = ImageCarouselAdapter(layoutInflater, it.photoUrlList)
        })


        arguments?.getParcelable<GasStation>(Const.KEY_GAS_STATION)?.let {
            viewModel.start(it)
        }
    }
}