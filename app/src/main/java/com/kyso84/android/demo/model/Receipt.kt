package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.NamedField
import java.util.Calendar
import java.util.Currency
import java.util.GregorianCalendar
import java.util.Locale
import java.util.TimeZone

@Entity(tableName = "receipt",
//        foreignKeys = arrayOf(
//                ForeignKey(entity = Car::class,
//                        parentColumns = arrayOf("id"),
//                        childColumns = arrayOf("carId"),
//                        onDelete = ForeignKey.CASCADE),
//                ForeignKey(entity = GasStation::class,
//                        parentColumns = arrayOf("id"),
//                        childColumns = arrayOf("gasStationId"),
//                        onDelete = ForeignKey.CASCADE)
//        ),
        indices = arrayOf(
                Index(value = ["carId"]),
                Index(value = ["gasStationId"])

        )
)
data class Receipt @JvmOverloads constructor(
        @PrimaryKey
        @SerializedName("_id")
        var id: String = "",

        // Context
        var carId: String? = null,
        @NamedField(R.string.car_name)
        var carName: String = "",
        var gasStationId: String? = null,
        @NamedField(R.string.gas_station_name)
        var gasStationName: String = "",

        // Meta data
        @NamedField(R.string.gas_type)
        var gasType: GasType = GasType.REGULAR,
        @NamedField(R.string.distance_unit)
        var distanceUnit: DistanceUnit = DistanceUnit.KILOMETER,
        @NamedField(R.string.volume_unit)
        var volumeUnit: VolumeUnit = VolumeUnit.LITER,
        @NamedField(R.string.currency)
        var currencyCode: String = Currency.getInstance(Locale.getDefault()).currencyCode,

        // Values
        @NamedField(R.string.date)
        var timeStamp: Long = System.currentTimeMillis(),
        var timeZoneId: String = TimeZone.getDefault().id,
        @NamedField(R.string.odometer)
        var odometer: Double = 0.0,
        @NamedField(R.string.volume)
        var volume: Double = 0.0,
        @NamedField(R.string.price)
        var price: Double = 0.0,
        @NamedField(R.string.total)
        var total: Double = 0.0,

        // Extra
        @NamedField(R.string.note)
        var note: String = ""

) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readString(),
            parcel.readString() ?: "",
            parcel.readString(),
            parcel.readString() ?: "",
            parcel.readParcelable(GasType::class.java.classLoader) ?: GasType.REGULAR,
            parcel.readParcelable(DistanceUnit::class.java.classLoader) ?: DistanceUnit.KILOMETER,
            parcel.readParcelable(VolumeUnit::class.java.classLoader) ?: VolumeUnit.LITER,
            parcel.readString() ?: "",
            parcel.readLong(),
            parcel.readString() ?: "",
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString() ?: "") {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(carId)
        parcel.writeString(carName)
        parcel.writeString(gasStationId)
        parcel.writeString(gasStationName)
        parcel.writeParcelable(gasType, flags)
        parcel.writeParcelable(distanceUnit, flags)
        parcel.writeParcelable(volumeUnit, flags)
        parcel.writeString(currencyCode)
        parcel.writeLong(timeStamp)
        parcel.writeString(timeZoneId)
        parcel.writeDouble(odometer)
        parcel.writeDouble(volume)
        parcel.writeDouble(price)
        parcel.writeDouble(total)
        parcel.writeString(note)
    }

    fun getTimeZone(): TimeZone = TimeZone.getTimeZone(timeZoneId)

    fun getCalendar(): Calendar {
        val calendar = GregorianCalendar.getInstance(getTimeZone())
        calendar.timeInMillis = timeStamp
        return calendar
    }

    fun getCurrency(): Currency = Currency.getInstance(currencyCode)
            ?: Currency.getInstance(Locale.getDefault())

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Receipt> {
        override fun createFromParcel(parcel: Parcel): Receipt {
            return Receipt(parcel)
        }

        override fun newArray(size: Int): Array<Receipt?> {
            return arrayOfNulls(size)
        }
    }

    fun getOdometerText(): String = if (this.odometer != 0.0) this.odometer.toString() else ""
    fun getVolumeText(): String = if (this.volume != 0.0) this.volume.toString() else ""
    fun getPriceText(): String = if (this.price != 0.0) this.price.toString() else ""
    fun getTotalText(): String = if (this.total != 0.0) this.total.toString() else ""


    fun addId(newId: String?): Receipt = Receipt(newId
            ?: "", carId, carName, gasStationId, gasStationName, gasType, distanceUnit, volumeUnit, currencyCode, timeStamp, timeZoneId, odometer, volume, price, total, note)

}