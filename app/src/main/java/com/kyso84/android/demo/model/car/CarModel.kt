package com.kyso84.android.demo.model.car

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CarModel(
    @SerializedName("model_name")
    val name: String,
    @SerializedName("model_make_id")
    val makeId: String,
    var year: String
) : Parcelable, Comparable<CarModel> {
    override fun compareTo(other: CarModel): Int {
        val res = name.compareTo(other.name)
        if (res == 0) {
            return makeId.compareTo(other.makeId)
        }
        return res
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(makeId)
        parcel.writeString(year)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return name
    }

    companion object CREATOR : Parcelable.Creator<CarModel> {
        override fun createFromParcel(parcel: Parcel): CarModel {
            return CarModel(parcel)
        }

        override fun newArray(size: Int): Array<CarModel?> {
            return arrayOfNulls(size)
        }
    }
}