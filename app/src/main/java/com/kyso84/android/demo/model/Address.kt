package com.kyso84.android.demo.model

import android.os.Parcel
import android.os.Parcelable

data class Address @JvmOverloads constructor(
        val formatted: String = "",

        var streetNumber: String = "",
        var streetName: String = "",
        var city: String = "",
        var zipCode: String = "",
        var state: String = "",
        var country: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(formatted)
        parcel.writeString(streetNumber)
        parcel.writeString(streetName)
        parcel.writeString(city)
        parcel.writeString(zipCode)
        parcel.writeString(state)
        parcel.writeString(country)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }
}