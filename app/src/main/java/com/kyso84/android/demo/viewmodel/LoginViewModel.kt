package com.kyso84.android.demo.viewmodel

import com.kyso84.android.demo.core.base.BaseViewModel
import com.kyso84.android.demo.core.service.SessionService
import com.kyso84.android.demo.datasource.CarRepository
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val sessionService: SessionService) : BaseViewModel() {

    companion object {
        const val TAG = "LoginViewModel"
    }
}