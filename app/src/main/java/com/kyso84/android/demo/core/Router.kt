package com.kyso84.android.demo.core

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import com.kyso84.android.demo.R
import com.kyso84.android.demo.core.service.BillingService
import com.kyso84.android.demo.core.service.LocationService
import com.kyso84.android.demo.datasource.local.PreferenceService
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.OnBoardingConf
import com.kyso84.android.demo.model.Pack
import com.kyso84.android.demo.model.Receipt
import com.kyso84.android.demo.view.activity.CarCreatorActivity
import com.kyso84.android.demo.view.activity.ExportActivity
import com.kyso84.android.demo.view.activity.LoginActivity
import com.kyso84.android.demo.view.activity.MainActivity
import com.kyso84.android.demo.view.activity.OnBoardingActivity
import com.kyso84.android.demo.view.fragment.ReceiptFormFragmentDirections
import com.kyso84.android.demo.view.fragment.ReceiptListFragmentDirections
import com.quicklib.android.app.QuickRouter
import java.util.ArrayList
import java.util.Date

class Router(private val preferenceService: PreferenceService, private val billingService: BillingService, private val locationService: LocationService) : QuickRouter() {

    // ACTIVITIES //////////////////////////////////////////////////////////////////////////////////

    fun openOnBoarding(activity: Activity, vararg onBoardingConf: OnBoardingConf) {
        val intent = Intent(activity, OnBoardingActivity::class.java)
        if (onBoardingConf.isNotEmpty()) {
            val confList = arrayListOf<OnBoardingConf>(*onBoardingConf)
            intent.putExtra(Const.KEY_ON_BOARDING_CONF, confList)
            startActivityForResult(activity, Const.REQUEST_ON_BOARDING, intent)
        } else {
        }
    }

    fun openLogin(activity: Activity) {
        val intent = Intent(activity, LoginActivity::class.java)
        startActivityForResult(activity, Const.REQUEST_LOGIN, intent)
    }

    fun openCarCreator(activity: Activity) {
        val intent = Intent(activity, CarCreatorActivity::class.java)
        startActivityForResult(activity, Const.REQUEST_CAR_CREATOR, intent)
    }

    fun openMain(activity: Activity, bundle: Bundle? = null) {
        val intent = Intent(activity, MainActivity::class.java)
        bundle?.let {
            intent.putExtras(bundle)
        }
        startActivity(activity, intent)
    }

    fun openExport(activity: Activity, successAction: () -> Unit, vararg receipt: Receipt) {
        val promoMessage = activity.getString(R.string.promo_export_pack, activity.getString(Pack.EXPORT_PACK.title), "0.99$")
        billingNavigation(view = activity.findViewById(android.R.id.content), pack = Pack.EXPORT_PACK, promoMessage = promoMessage, action = {
            val intent = Intent(activity, ExportActivity::class.java)
            intent.putParcelableArrayListExtra(Const.KEY_RECEIPT_LIST, ArrayList(receipt.toList())  )
            startActivity(activity, intent)
            successAction.invoke()
        })
    }


    // NAVIGATION //////////////////////////////////////////////////////////////////////////////////

    fun showCarForm(view: View) {
        val action = ReceiptListFragmentDirections.receiptListFragmentToCarFormFragment()
        Navigation.findNavController(view).navigate(action)
    }

    fun showReceiptCreator(view: View, car: Car? = null, gasStation: GasStation? = null) {
        conditionalNavigation(view, R.string.need_a_car, { car != null }, {
            val action = ReceiptListFragmentDirections.receiptListFragmentToReceiptFormFragment()
            action.car = car
            action.gasStation = gasStation
            Navigation.findNavController(view).navigate(action)
        }, {
            showCarForm(view)
        })
    }

    fun showCarPicker(view: View, successAction: () -> Unit) {
        val context = view.context
        val promoMessage = context.getString(R.string.promo_car_pack, context.getString(Pack.CAR_PACK.title), "0.99$")
        billingNavigation(view = view, pack = Pack.CAR_PACK, promoMessage = promoMessage, action = {
            Toast.makeText(view.context, "OPEN CAR PICKER", Toast.LENGTH_SHORT).show()
            successAction.invoke()
        })
    }

    fun showReceiptEditor(view: View, receipt: Receipt) {
        val action = ReceiptListFragmentDirections.receiptListFragmentToReceiptFormFragment()
        action.receipt = receipt
        Navigation.findNavController(view).navigate(action)
    }

    fun showGasStationViewer(view:View,gasStation: GasStation?) {
        gasStation?.let{
            val action = ReceiptFormFragmentDirections.receiptFormFragmentToGasStationViewer(it)
            Navigation.findNavController(view).navigate(action)
        }
    }
    // DIALOG //////////////////////////////////////////////////////////////////////////////////////

    fun promptLocationPermissionDialog(fragment: Fragment, listener: LocationService.PermissionCallback) {
        val activity = fragment.context as Activity
        if (!preferenceService.isLocationRefused) {
            val alertBuilder = AlertDialog.Builder(activity)
            alertBuilder.setMessage(com.kyso84.android.demo.R.string.incitement_location_permission)
            alertBuilder.setNegativeButton(com.kyso84.android.demo.R.string.later) { dialog, which ->
                listener.onPermissionDeclined()
                dialog.dismiss()
            }
            alertBuilder.setPositiveButton(com.kyso84.android.demo.R.string.yes) { dialog, which ->
                locationService.askLocation(fragment, listener, true)
                dialog.dismiss()
            }
            alertBuilder.setNeutralButton(com.kyso84.android.demo.R.string.never) { dialog, which ->
                preferenceService.isLocationRefused = true
                listener.onPermissionDeclined()
                dialog.dismiss()
            }
            alertBuilder.show()
        }
    }

    fun promptDateTimeDialog(fragment: Fragment, receipt: Receipt? = null) {
        val context = fragment.context
        context?.let {
            var dateTimeFragment = fragment.childFragmentManager.findFragmentByTag(Const.FRAGMENT_TAG_DATETIME) as SwitchDateTimeDialogFragment?
            if (dateTimeFragment == null) {
                dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                        it.getString(R.string.app_name),
                        it.getString(R.string.ok),
                        it.getString(R.string.cancel),
                        it.getString(R.string.now) // Optional
                )
                dateTimeFragment.show(fragment.childFragmentManager, Const.FRAGMENT_TAG_DATETIME)
            }
            receipt?.let {
                dateTimeFragment?.setTimeZone(it.getTimeZone())
            }
            dateTimeFragment?.set24HoursMode(true)
            dateTimeFragment?.startAtCalendarView()
            dateTimeFragment?.setOnButtonClickListener(object : SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener {
                override fun onPositiveButtonClick(date: Date) {
                    val intent = Intent()
                    intent.putExtra(Const.KEY_TIMESTAMP, date.time)
                    fragment.onActivityResult(Const.REQUEST_DATETIME_PICKER, Activity.RESULT_OK, intent)
                }

                override fun onNegativeButtonClick(date: Date) {}

                override fun onNeutralButtonClick(date: Date) {
                    val intent = Intent()
                    intent.putExtra(Const.KEY_TIMESTAMP, System.currentTimeMillis())
                    fragment.onActivityResult(Const.REQUEST_DATETIME_PICKER, Activity.RESULT_OK, intent)
                }
            })
        }
    }


    // UTILS ///////////////////////////////////////////////////////////////////////////////////////

    private fun billingNavigation(view: View, pack: Pack, promoMessage: String, action: () -> Unit) {
        conditionalNavigation(view, promoMessage, { billingService.isPackPurchased(pack) }, { action.invoke() }, { Toast.makeText(view.context, "REDIRECT TO PROMOTIONS", Toast.LENGTH_SHORT).show() })
    }

    private fun conditionalNavigation(view: View, @StringRes promoMessageId: Int, condition: () -> Boolean, validAction: () -> Unit, invalidAction: () -> Unit) {
        conditionalNavigation(view, view.context.getString(promoMessageId), condition, validAction, invalidAction)
    }

    private fun conditionalNavigation(view: View, promoMessage: String, condition: () -> Boolean, validAction: () -> Unit, invalidAction: () -> Unit) {
        when {
            condition.invoke() -> validAction.invoke()
            view.context is Activity -> {
                val dialogBuilder = AlertDialog.Builder(view.context)
                dialogBuilder.setMessage(promoMessage)
                dialogBuilder.setPositiveButton(R.string.ok) { dialog, which ->
                    invalidAction.invoke()
                    dialog.dismiss()
                }
                dialogBuilder.setNegativeButton(R.string.later) { dialog, which -> dialog.dismiss() }
                dialogBuilder.show()
            }
            else -> {
                Toast.makeText(view.context, promoMessage, Toast.LENGTH_SHORT).show()
                Snackbar.make(view, promoMessage, Snackbar.LENGTH_SHORT).setAction(R.string.ok) {
                    invalidAction.invoke()
                }.show()
            }
        }
    }

    fun back(view: View) {
        if (view.context is MainActivity) {
            val navController: NavController? = Navigation.findNavController(view)
            navController?.let {
                it.popBackStack()
            } ?: run {
                (view.context as Activity).finish()
            }
        } else {
            (view.context as Activity).finish()
        }
    }

}
