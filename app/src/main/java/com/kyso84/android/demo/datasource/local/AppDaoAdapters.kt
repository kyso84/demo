package com.kyso84.android.demo.datasource.local

import androidx.room.TypeConverter
import com.kyso84.android.demo.App
import com.kyso84.android.demo.model.* // ktlint-disable no-wildcard-imports
import java.util.* // ktlint-disable no-wildcard-imports

class AppDaoAdapters {

    private val gson = App.get().appComponent.getGson()

    @TypeConverter
    fun readDistanceUnit(text: String): DistanceUnit = DistanceUnit.valueOf(text.toUpperCase())

    @TypeConverter
    fun writeDistanceUnit(distanceUnit: DistanceUnit): String = distanceUnit.toString().toLowerCase()

    @TypeConverter
    fun readVolumeUnit(text: String): VolumeUnit = VolumeUnit.valueOf(text.toUpperCase())

    @TypeConverter
    fun writeVolumeUnit(volumeUnit: VolumeUnit): String = volumeUnit.toString().toLowerCase()

    @TypeConverter
    fun readCurrency(text: String): Currency = Currency.getInstance(text)

    @TypeConverter
    fun writeCurrency(currency: Currency): String = currency.currencyCode

    @TypeConverter
    fun readGasType(text: String): GasType = GasType.valueOf(text.toUpperCase())

    @TypeConverter
    fun writeGasType(gasType: GasType): String = gasType.toString().toLowerCase()

    @TypeConverter
    fun readGasTypeGroup(text: String): GasTypeGroup = GasTypeGroup.valueOf(text.toUpperCase())

    @TypeConverter
    fun writeGasTypeGroup(gasTypeGroup: GasTypeGroup): String = gasTypeGroup.toString().toLowerCase()

    @TypeConverter
    fun readTimeZone(text: String): TimeZone = TimeZone.getTimeZone(text)

    @TypeConverter
    fun writeTimeZone(timeZone: TimeZone): String = timeZone.id

    @TypeConverter
    fun readList(text: String): List<String> = gson.fromJson(text, listOf<String>()::class.java)

    @TypeConverter
    fun writeList(list: List<String>): String = gson.toJson(list)

    @TypeConverter
    fun readArrayList(text: String): ArrayList<String> = gson.fromJson(text, arrayListOf<String>()::class.java)

    @TypeConverter
    fun writeArrayList(list: ArrayList<String>): String = gson.toJson(list)

    @TypeConverter
    fun readAddress(text: String): Address = gson.fromJson(text, Address::class.java)

    @TypeConverter
    fun writeAddress(address: Address): String = gson.toJson(address)

//    @TypeConverter
//    fun readCar(text: String): Car? = if (text.isNotEmpty()) gson.fromJson<Car>(text, Car::class.java) else null
//
//    @TypeConverter
//    fun writeCar(car: Car?): String = if (car != null) gson.toJson(car) else ""
//
//    @TypeConverter
//    fun readGasStation(text: String): GasStation? = if (text.isNotEmpty()) gson.fromJson<GasStation>(text, GasStation::class.java) else null
//
//    @TypeConverter
//    fun writeGasStation(gasStation: GasStation?): String = if (gasStation != null) gson.toJson(gasStation) else ""
}