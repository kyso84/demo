package com.kyso84.android.demo.core

class Const {
    companion object {


        // Requests
        const val REQUEST_PERMISSION_LOCATION = 100
        const val REQUEST_DATETIME_PICKER = 101
        const val REQUEST_ON_BOARDING = 102
        const val REQUEST_LOGIN = 103
        const val REQUEST_CAR_CREATOR = 104
        const val REQUEST_CONTACT_ACCESS = 105

        // Conf keys
        const val CONF_ENABLE_ON_BOARDING = "enableOnBoarding"
        const val CONF_SPLASH_SCREEN_DELAY = "splashScreenDelay"

        // Fragment tags
        const val FRAGMENT_TAG_DATETIME = "fragmentDateTime"
        const val FRAGMENT_TAG_CAR_FORM = "fragmentCarForm"

        // Bundle Keys
        const val KEY_CURRENCY = "currency"
        const val KEY_VOLUME = "volume"
        const val KEY_VOLUME_UNIT = "volumeUnit"
        const val KEY_DISTANCE = "distance"
        const val KEY_DISTANCE_UNIT = "distanceUnit"
        const val KEY_LAST_GAS_TYPE = "lastGasType"
        const val KEY_RECEIPT = "receipt"
        const val KEY_SYNC_ENABLED = "syncEnabled"
        const val KEY_LOCATION_REFUSED = "locationRefused"
        const val KEY_TIMESTAMP = "timestamp"
        const val KEY_API = "apiKey"
        const val KEY_RECEIPT_LIST = "receiptList"
        const val KEY_FIELD_LIST = "fieldList"
        const val KEY_FORMAT = "format"
        const val KEY_RECIPIENT_LIST = "recipientList"
        const val KEY_LAST_EXPORT_FORMAT = "lastExportFormat"
        const val KEY_LAST_EXPORT_FIELDS = "lastExportFields"
        const val KEY_LAST_EXPORT_RECIPIENTS = "lastExportRecipients"
        const val KEY_ON_BOARDING_CONF = "onBoardingConf"
        const val KEY_LAST_CAR_ID = "lastCarId"
        const val KEY_CAR = "car"
        const val KEY_START_COUNT = "startCount"
        const val KEY_ON_BOARDING_SHOWN = "onBoardingShown"
        const val KEY_PUBLISHED_VERSION_CODE = "publishedVersionCode"
        const val KEY_USER = "user"
        const val KEY_NAV_ID = "navId"
        const val KEY_SELECTED_FIELDS = "selectedFields"
        const val KEY_FIRESTORE = "firestore"
        const val KEY_FIREBASE = "firebase"
        const val KEY_OPEN = "open"
        const val KEY_RESTRICTED = "restricted"
        const val KEY_GAS_STATION: String = "gasStation"
        const val KEY_MAPS_API_KEY: String = "mapsApiKey"
    }
}