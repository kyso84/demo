package com.kyso84.android.demo.datasource.remote

import java.lang.Exception

class ApiException(message:String):Exception(message){
    constructor(appResponse: AppResponse<*>):this(appResponse.errorMessage)
}