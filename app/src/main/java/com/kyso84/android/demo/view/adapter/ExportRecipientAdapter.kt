package com.kyso84.android.demo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kyso84.android.demo.core.SimpleItemTouchHelperCallback
import com.kyso84.android.demo.model.Contact
import com.kyso84.android.demo.model.ExportFormat
import kotlinx.android.synthetic.main.part_export_field_row.view.*

class ExportRecipientAdapter(private val layoutInflater: LayoutInflater, private var items: MutableList<Contact> = mutableListOf()) : RecyclerView.Adapter<ExportRecipientViewHolder>() {

    private var listener: OnSelectionChangedListener? = null

    interface OnSelectionChangedListener {
        fun onSelectionChanged(contactList: List<Contact>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExportRecipientViewHolder = ExportRecipientViewHolder(layoutInflater, parent)

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ExportRecipientViewHolder, position: Int) {
        val contact = items[position]
        holder.bind(contact)
        holder.binding.partExportRecipientRowClear.setOnClickListener {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun setOnSelectionChangedListener(listener: OnSelectionChangedListener) {
        this.listener = listener
    }
}