package com.kyso84.android.demo.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kyso84.android.demo.model.Car
import com.kyso84.android.demo.model.GasStation
import com.kyso84.android.demo.model.Receipt

@Database(entities = [Receipt::class, Car::class, GasStation::class], version = 1, exportSchema = false)
@TypeConverters(AppDaoAdapters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getAppDao(): AppDao
}