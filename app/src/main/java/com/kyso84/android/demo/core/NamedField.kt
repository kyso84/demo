package com.kyso84.android.demo.core

import androidx.annotation.StringRes
import java.lang.annotation.RetentionPolicy


// https://medium.com/@elye.project/annotation-tutorial-for-dummies-in-kotlin-1da864acc442
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class NamedField(@StringRes val aliasId:Int)