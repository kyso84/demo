package com.kyso84.android.demo.datasource.repo

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import com.kyso84.android.demo.datasource.ContactRepository
import com.kyso84.android.demo.model.Contact
import com.quicklib.android.network.strategy.LocalDataOnlyStrategy
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred

class DeviceRepository(private val context: Context) : ContactRepository {


    private fun query(text: String = ""): Cursor? {
        var selection: String? = null
        val selectionArgs: Array<String>? = null
        if (text.isNotEmpty()) {
            selection = "${ContactsContract.CommonDataKinds.Email.DATA} LIKE '%$text%' or ${ContactsContract.Contacts.DISPLAY_NAME} LIKE '%$text%'"
        }
        return context.contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, selection, selectionArgs, null)
    }

    @Throws(Exception::class)
    private fun getContactFromCursor(cursor: Cursor?): List<Contact> {
        val list = mutableListOf<Contact>()
        cursor?.let {
            while (it.moveToNext()) {

                val contactIdIndex = it.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)
                val contactNameIndex = it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                val contactEmailIndex = it.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)

                val contactId = cursor.getLong(contactIdIndex)
                val contactName = cursor.getString(contactNameIndex)
                val contactEmail = cursor.getString(contactEmailIndex)

                val contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId)
                val contactAvatarUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)

                val contact = Contact(name = contactName, email = contactEmail, avatarUri = contactAvatarUri)
                list.add(contact)
            }
        }
        return list
    }


    override fun getContactList() = searchContact("")

    override fun searchContact(text: String) = object : LocalDataOnlyStrategy<List<Contact>>() {
        override suspend fun readData(): Deferred<List<Contact>> {
            var cursor: Cursor? = null
            try {
                cursor = query(text)
                return CompletableDeferred(getContactFromCursor(cursor))
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                cursor?.close()
            }
            return CompletableDeferred(listOf())
        }
    }.asLiveData()

    override fun getContactListFromEmailList(emailList: List<String>) = object : LocalDataOnlyStrategy<List<Contact>>() {
        override suspend fun readData(): Deferred<List<Contact>> {
            var cursor: Cursor? = null
            try {
                cursor = query("")
                return CompletableDeferred(getContactFromCursor(cursor).filter { emailList.contains(it.email) })
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                cursor?.close()
            }
            return CompletableDeferred(listOf())
        }
    }.asLiveData()
}